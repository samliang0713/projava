package edu.uchicago.gerber._03objects.P8_8;

public class Driver {
    public static void main(String[] args) {
        VotingMachine machine = new VotingMachine();
        machine.voteForDemocrat();
        machine.voteForRepublic();
        machine.voteForDemocrat();

        System.out.println("Republic Vote: " + machine.getRepublicanVote());
        System.out.println("Democrat Vote: " + machine.getDemocratVote());

        System.out.println("--------");

        machine.clear();
        machine.voteForDemocrat();
        
        System.out.println("Republic Vote: " + machine.getRepublicanVote());
        System.out.println("Democrat Vote: " + machine.getDemocratVote());

    }
}
