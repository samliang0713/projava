package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int democratVote;
    private int republicanVote;

    public VotingMachine() {
        this.democratVote = 0;
        this.republicanVote = 0;
    }

    public void voteForDemocrat() {
        this.democratVote++;
    }

    public void voteForRepublic() {
        this.republicanVote++;
    }

    public void clear() {
        this.democratVote = 0;
        this.republicanVote = 0;
    }

    public int getDemocratVote() {
        return this.democratVote;
    }

    public int getRepublicanVote() {
        return this.republicanVote;
    }
}
