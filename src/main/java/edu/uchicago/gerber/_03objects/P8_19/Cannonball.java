package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double xPos;
    private double yPos;
    private double xVelocity;
    private double yVelocity;

    public Cannonball(double xPos) {
        this.xPos = xPos;
        this.yPos = 0;
        this.xVelocity = 0;
        this.yVelocity = 0;
    }

    public void move(double sec) {
        this.xPos = xPos + xVelocity * sec;
        this.yPos = yPos + yVelocity * sec;
        this.yVelocity = yVelocity - 9.81 * sec;
    }

    public double getX() {
        return this.xPos;
    }

    public double getY() {
        return this.yPos;
    }

    public void shoot(double angle, double intialVelocity) {
        this.xVelocity = intialVelocity * Math.cos(angle);
        this.yVelocity = intialVelocity * Math.sin(angle);

        do {
            move(0.1);
            System.out.println("Current x Pos: " + this.getX() + " / " + "Current y Pos: " + this.getY());
        } while (yPos > 0);
    }

}