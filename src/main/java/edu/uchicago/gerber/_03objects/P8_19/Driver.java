package edu.uchicago.gerber._03objects.P8_19;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Initialization
        boolean angleExists = false;
        double angle = -1;

        //Prompt angle input
        while (!angleExists) {
            try {
                System.out.print("Please enter an angle between 0 and PI / 2: ");

                angle = scanner.nextDouble();
                if (angle < 0 || angle > Math.PI / 2) throw new IllegalArgumentException();
                angleExists = true;
            } catch (IllegalArgumentException e) {
                System.out.println("Please enter an angle between 0 and PI / 2");
            } catch (Exception e) {
                System.out.println("Please enter an angle between 0 and PI / 2");
                scanner.next();
            }
        }

        //Initialization
        boolean initialVelocityExists = false;
        double initialVelocity = -1;

        //Prompt velocity input
        while (!initialVelocityExists) {
            try {
                System.out.print("Please enter a initial velocity: ");
                initialVelocity = scanner.nextDouble();
                if (initialVelocity < 0) throw new IllegalArgumentException();
                initialVelocityExists = true;

            } catch (IllegalArgumentException e) {
                System.out.println("Please enter a positive double number");
            } catch (Exception e) {
                System.out.println("Please enter a positive double number");
                scanner.next();
            }
        }

        Cannonball cannonball = new Cannonball(0.5);

        cannonball.shoot(angle, initialVelocity);

    }
}