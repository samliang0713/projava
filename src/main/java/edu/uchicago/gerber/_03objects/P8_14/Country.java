package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    private String name;
    private double area;
    private double population;

    public Country(String name, double area, double population) {
        this.name = name;
        this.area = area;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public double getArea() {
        return area;
    }

    public double getPopulation() {
        return population;
    }

    public String toString() {
        return this.name + " with area " + this.area + " and population " + this.population;
    }
}