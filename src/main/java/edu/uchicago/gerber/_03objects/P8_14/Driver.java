package edu.uchicago.gerber._03objects.P8_14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) throws FileNotFoundException {


        File file = new File("input.txt");
        Scanner scanner = new Scanner(file);
        ArrayList<Country> countryList = new ArrayList<>();


        while (scanner.hasNextLine()) {
            String name = scanner.next();
            double area = scanner.nextDouble();
            double pop = scanner.nextDouble();
            countryList.add(new Country(name, area, pop));
        }


        // Initialization
        Country largestAreaCountry = countryList.get(0);
        Country largestPopCountry = countryList.get(0);
        Country largestPopDensityCountry = countryList.get(0);

        for (Country country : countryList) {
            largestAreaCountry = largestAreaCountry.getArea() > country.getArea() ? largestAreaCountry : country;
            largestPopCountry = largestPopCountry.getPopulation() > country.getPopulation() ? largestPopCountry : country;
            largestPopDensityCountry = (largestPopDensityCountry.getPopulation() / largestPopDensityCountry.getArea()) >
                    (country.getPopulation() / country.getArea()) ? largestPopDensityCountry : country;
        }

        System.out.println("Country with largest area : " + largestAreaCountry.toString());
        System.out.println("Country with largest population : " + largestPopCountry.toString());
        System.out.println("Country with largest population density : " + largestPopDensityCountry.toString());
    }
}