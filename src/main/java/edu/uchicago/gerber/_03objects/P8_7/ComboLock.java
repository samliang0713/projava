package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    // Secrets
    private int secret1;
    private int secret2;
    private int secret3;

    // Number the dial pointing to. Always between 0 and 39.
    private int dial;

    // Inputs from user, initialized at -1. The third input is at the dial.
    private int input1;
    private int input2;

    public ComboLock(int secret1, int secret2, int secret3) {
        this.secret1 = secret1;
        this.secret2 = secret2;
        this.secret3 = secret3;

        // Inputs from user, initialized at -1. The third input is at the dial.
        this.input1 = -1;
        this.input2 = -1;

        // Dial initialized at 0;
        this.dial = 0;
    }

    // Assume it also resets the input.
    public void reset() {
        this.input1 = -1;
        this.input2 = -1;
        this.dial = 0;
    }

    public void turnRight(int ticks) {
        // Save the number at the dial from the left turn
        if (this.input1 != -1) {
            this.input2 = this.dial;
        }

        this.dial = (this.dial + ticks) % 40;
    }

    public void turnLeft(int ticks) {
        // Save the number at the dial from the first right turn
        this.input1 = this.dial;
        // To ensure we have a non negative number
        this.dial = this.dial - ticks > 0 ? this.dial - ticks : 40 + this.dial - ticks % 40;
    }

    public boolean open() {
        System.out.println("input1 = " + this.input1);
        System.out.println("input2 = " + this.input2);
        System.out.println("input3 = " + this.dial);
        return this.input1 == this.secret1 && this.input2 == this.secret2 && this.secret3 == this.dial;
    }


}