package edu.uchicago.gerber._03objects.P8_7;

public class Driver {
    public static void main(String[] args) {
        ComboLock combolock = new ComboLock(1, 2, 3);

        System.out.println(combolock.open());

        combolock.turnRight(1);
        combolock.turnLeft(79);
        combolock.turnRight(41);

        System.out.println(combolock.open());

    }
}
