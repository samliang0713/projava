package edu.uchicago.gerber._03objects.P8_16;

public class Driver {
    public static void main(String[] args) {
        Message email1 = new Message("Harry", "Sam", "How have you been?");
        System.out.println(email1.toString());

        System.out.println("--------------");
        email1.append("I hope you are doing well!");
        System.out.println(email1.toString());


        Mailbox mailbox = new Mailbox();

        Message email2 = new Message("Joe", "Bruin", "Hey what's up?");

        mailbox.addMessage(email1);
        mailbox.addMessage(email2);

        System.out.println("--------------");
        System.out.println(mailbox.getMessage(0).toString());
        System.out.println("--------------");
        System.out.println(mailbox.getMessage(1).toString());

        mailbox.removeMessage(0);
        System.out.println("--------------");
        System.out.println(mailbox.getMessage(0).toString());

    }
}