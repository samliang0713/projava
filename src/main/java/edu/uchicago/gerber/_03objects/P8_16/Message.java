package edu.uchicago.gerber._03objects.P8_16;

public class Message {

    private String sender;
    private String recipient;
    private String text;

    public Message(String sender, String recipient, String text) {
        this.sender = sender;
        this.recipient = recipient;
        this.text = text;
    }

    public void append(String extraText) {
        this.text = this.text + "\n" + extraText;
    }

    public String toString() {
        return "From: " + this.sender + "\n" +
                "To: " + this.recipient + "\n" +
                this.text;
    }
}