package edu.uchicago.gerber._03objects.P8_16;

import java.util.ArrayList;

public class Mailbox {
    private ArrayList<Message> inbox;

    public Mailbox() {
        this.inbox = new ArrayList<Message>();
    }

    public void addMessage(Message m) {
        this.inbox.add(m);
    }

    public Message getMessage(int i) throws IndexOutOfBoundsException {
        if (i > this.inbox.size()) {
            throw new IndexOutOfBoundsException("Sorry, the index number is larger than the inbox size");
        }
        return this.inbox.get(i);
    }

    public void removeMessage(int i) {
        if (i < this.inbox.size()) {
            this.inbox.remove(i);
        }
    }
}