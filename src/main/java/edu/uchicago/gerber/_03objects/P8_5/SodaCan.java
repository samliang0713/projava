package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private int height;
    private int radius;

    public SodaCan(int radius, int height) {
        this.height = height;
        this.radius = radius;
    }

    public double getSurfaceArea() {
        return radius * radius * Math.PI * 2 + height * radius * 2 * Math.PI;
    }

    public double getVolume() {
        return radius * radius * Math.PI * height;
    }
}
