package edu.uchicago.gerber._03objects.P8_5;

public class Driver {
    public static void main(String[] args) {
        SodaCan can = new SodaCan(2, 3);
        System.out.printf("Surface Area: %.2f %n", can.getSurfaceArea());
        System.out.printf("Volume: %.2f", can.getVolume());
    }
}
