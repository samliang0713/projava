package edu.uchicago.gerber._03objects.P8_6;

public class Driver {
    public static void main(String[] args) {
        Car car = new Car(15.3);

        System.out.println(car.getGasLevel());

        car.addGas(21.2);

        System.out.println(car.getGasLevel());

        car.drive(15.2);

        System.out.println(car.getGasLevel());
    }
}
