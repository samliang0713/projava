package edu.uchicago.gerber._03objects.P8_1;

public class Driver {
    public static void main(String[] args) {
        Microwave microwave = new Microwave();
        microwave.start();

        microwave.increaseThirty();
        microwave.increaseThirty();
        microwave.switchPower();
        microwave.start();

        microwave.reset();
        microwave.start();
    }
}
