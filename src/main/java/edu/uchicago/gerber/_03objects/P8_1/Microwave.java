package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {

    private int power;
    private int time;

    public Microwave() {
        // The default value of power is 1
        this.power = 1;
        // The default value of time is 0
        this.time = 0;
    }


    public void reset() {
        this.time = 0;
        this.power = 1;
    }

    public void increaseThirty() {
        this.time += 30;
    }

    public void switchPower() {
        this.power = this.power == 1 ? 2 : 1;
    }

    public void start() {
        System.out.println("Cooking for " + this.time + " seconds at level " + this.power);
    }
}
