package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;

public class SpecifyDriverViewer {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(900,900);
        frame.setTitle("Specify Circle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(new SpecifyCircle());
        frame.setVisible(true);
    }
}
