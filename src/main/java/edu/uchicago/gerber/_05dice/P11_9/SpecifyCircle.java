package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SpecifyCircle extends JComponent {
    private double centerX;
    private double centerY;
    private double edgeX;
    private double edgeY;
    private boolean isFirstPress;



    public SpecifyCircle() {
        isFirstPress = true;
        addMouseListener(new ClickListener());
    }

    class ClickListener implements MouseListener {

        @Override
        public void mousePressed(MouseEvent e) {
            if (isFirstPress) {
                centerX = e.getX();
                centerY = e.getY();
                isFirstPress = !isFirstPress;
//                System.out.println("first");
            }
            else {
                edgeX = e.getX();
                edgeY = e.getY();
                isFirstPress = !isFirstPress;
//                System.out.println("second!");
                Graphics g = getGraphics();
                double radius = Math.sqrt(Math.pow(edgeX - centerX,2)+Math.pow(edgeY-centerY,2));
//                System.out.println("center: " + centerX + " / " + centerY);
//                System.out.println("edge: " + edgeX + " / " + edgeY);
//                System.out.println(radius);
                g.drawOval((int) (centerX-radius), (int)(centerY-radius), (int) (2*radius),(int) (2*radius));

            }
        }


        // Do nothing
        @Override
        public void mouseClicked(MouseEvent e) {

        }


        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
