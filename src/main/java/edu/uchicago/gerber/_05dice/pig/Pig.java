package edu.uchicago.gerber._05dice.pig;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Pig extends JFrame {
    private int playerTotal;
    private int computerTotal;
    private final JPanel playerPanel;

    private int playerCurrentRound;
    private int computerCurrentRound;
    private boolean didPlayerRoll;

    public Pig() {
        this.playerTotal = 0;
        this.computerTotal = 0;
        playerPanel = new JPanel();
        createPlayerPanel();
        setSize(300, 400);
        System.out.println("Player plays first!");
        didPlayerRoll = false;
    }

    private boolean checkEndGame() {
        return playerTotal >= 100 || computerTotal >= 100;
    }

    private void printWinner() {
        if (playerTotal >= 100) {
            System.out.println("******************************");
            System.out.println("*        Player Wins!        *");
            System.out.println("******************************");
        }

        if (computerTotal >= 100) {
            if (computerTotal >= 100) {
                System.out.println("******************************");
                System.out.println("*        Computer Wins!      *");
                System.out.println("******************************");
            }
        }
    }

    private void computerAction() {

        if (!checkEndGame()) {
            // Computer chooses to make 1-10 rolls
            int numRolls = (int) (Math.random() * 10) + 1;

            while (numRolls > 0) {

                int roll = (int) (Math.random() * 6) + 1;
                System.out.println("Computer rolls: " + roll);
                if (roll == 1) {
                    System.out.println("Computer throws a 1!");
                    computerCurrentRound = 0;
                    break;
                }

                computerCurrentRound += roll;
                if (computerTotal + computerCurrentRound >= 100) break;
                numRolls--;
            }

            System.out.println("Computer's round is over. Computer gains " + computerCurrentRound + " points this round");
            computerTotal += computerCurrentRound;
            computerCurrentRound = 0;

            didPlayerRoll = false;
            displayScore();

            // check scores again
            if (!checkEndGame()) {
                System.out.println("Player's turn: ");
            } else {
                printWinner();
            }
        }


    }

    private void displayScore() {
        System.out.println("+++++++++++++++++++++++");
        System.out.println("Player: " + playerTotal);
        System.out.println("Computer: " + computerTotal);
        System.out.println("+++++++++++++++++++++++");
    }


    private void playerRoll() {
        didPlayerRoll = true;
        if (!checkEndGame()) {
            int roll = (int) (Math.random() * 6) + 1;
            System.out.println("Player's roll is: " + roll);

            if (roll == 1) {
                playerCurrentRound = 0;
                playerHold();
                computerAction();
                return;
            }

            playerCurrentRound += roll;
            System.out.println("Player's current round total is: " + playerCurrentRound);
            if (playerCurrentRound + playerTotal >= 100) {
                playerTotal += playerCurrentRound;
                playerHold();
            }
        } else {
            printWinner();
        }


    }

    private void playerHold() {
        if (!checkEndGame()) {
                if(!didPlayerRoll) {
                    System.out.println("Player must at least roll once!");
                } else {
                    System.out.println("Player's current round is over. Player gains " + playerCurrentRound + " points this round.");
                    playerTotal += playerCurrentRound;
                    playerCurrentRound = 0;
                    System.out.println("----------------------------------");
                }
        } else {
            displayScore();
            printWinner();
        }
    }


    private void createPlayerPanel() {
        JButton roll = new JButton("Roll");
        JButton hold = new JButton("Hold");

        roll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playerRoll();
            }
        });

        hold.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playerHold();
                if(didPlayerRoll) computerAction();
            }
        });

        playerPanel.add(roll);
        playerPanel.add(hold);
        add(playerPanel);
    }


}
