package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;

public class BillFrameViewer {
    public static void main(String[] args) {
        JFrame frame = new BillFrame();
        frame.setTitle("Restaurant Bill");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);
    }
}
