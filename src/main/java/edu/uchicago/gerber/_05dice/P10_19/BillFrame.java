package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class BillFrame extends JFrame {
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 700;
    private static final double SALES_TAX = 0.08;


    private final ArrayList<Dish> dishList;
    private final ArrayList<Dish> typedDishList;
    private final JPanel addDishPanel;
    private final JPanel typeDishPanel;
    private final JTextArea bill;
    private double totalBeforeTax;
    private double totalSalesTax;


    public BillFrame() {
        totalBeforeTax = 0;
        bill = new JTextArea("");
        dishList = new ArrayList<>();
        addDishPanel = new JPanel();
        typedDishList = new ArrayList<>();

        typeDishPanel = new JPanel();

        createDishes();
        createAddDishPanel();
        createTypeDishPanel();
        createMainPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    public double getTotalBeforeTax() {
        return totalBeforeTax;
    }

    public void setTotalBeforeTax(double totalBeforeTax) {
        this.totalBeforeTax = totalBeforeTax;
    }

    public double getTotalSalesTax() {
        return totalSalesTax;
    }

    public void setTotalSalesTax(double totalSalesTax) {
        this.totalSalesTax = totalSalesTax;
    }


    private void updateBill() {
        bill.setText("");
        for (Dish dish : dishList) {
            if (dish.getCount() > 0)
                bill.append(dish.getCount() + " " + dish.getName() + " at " + dish.getPrice() + "\n");
        }
        for (Dish dish : typedDishList) {
            if (dish.getCount() > 0)
                bill.append(dish.getCount() + " " + dish.getName() + " at " + dish.getPrice() + "\n");
        }

        bill.append(String.format("Sales Taxes: %.2f %n", getTotalSalesTax()));
        bill.append(String.format("%nTotal: %.2f %n", getTotalBeforeTax() + getTotalSalesTax()));
        bill.append("------------\n");
        double tips = (getTotalSalesTax() + getTotalBeforeTax()) * 0.15;
        bill.append(String.format("Suggested Tips (15%%): %.2f %n", tips));
    }

    private void createDishes() {
        dishList.add(new Dish("coke", 1.30, 0));
        dishList.add(new Dish("fruit punch", 2.50, 0));
        dishList.add(new Dish("fried eggs", 2.50, 0));
        dishList.add(new Dish("omelette", 2.50, 0));
        dishList.add(new Dish("steak", 19.50, 0));
        dishList.add(new Dish("burger", 12.50, 0));
        dishList.add(new Dish("apple", 3.5, 0));
        dishList.add(new Dish("cheese cake", 5.5, 0));
        dishList.add(new Dish("apple pie", 8.5, 0));
        dishList.add(new Dish("pork ribs", 15.5, 0));

    }

    private void createAddDishPanel() {
        addDishPanel.setLayout(new GridLayout(5, 2));
        addDishPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        for (Dish dish : dishList) {
            JPanel dishPanel = new JPanel();
            dishPanel.add(new JLabel(dish.getName() + " : " + dish.getPrice()));
            dishPanel.add(dish.getAddItemButton());
            dishPanel.add(dish.getSubtractItemButton());
            addDishPanel.add(dishPanel);
        }
    }


    private void createTypeDishPanel() {
        JTextField itemNameField = new JTextField(5);
        JTextField priceField = new JTextField(5);
        JTextField quantityField = new JTextField(5);
        JButton addButton = new JButton("Add");

        typeDishPanel.add(new JLabel("Enter Name"));
        typeDishPanel.add(itemNameField);
        typeDishPanel.add(new JLabel("Enter Price"));
        typeDishPanel.add(priceField);
        typeDishPanel.add(new JLabel("Enter Quantity"));
        typeDishPanel.add(quantityField);
        typeDishPanel.add(addButton);
        typeDishPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              try {
                  double price = Double.parseDouble(priceField.getText());
                  int count = Integer.parseInt(quantityField.getText());
                  typedDishList.add(new Dish(itemNameField.getText(), price, count));
                  setTotalSalesTax(getTotalSalesTax() + (price * SALES_TAX) * count);
                  setTotalBeforeTax(getTotalBeforeTax() + price * count);
                  updateBill();
              } catch(Exception exception) {
                  exception.printStackTrace();
                }
            }
        });
    }

    private void createMainPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));


        mainPanel.add(addDishPanel);
        mainPanel.add(typeDishPanel);
        mainPanel.add(bill);
        add(mainPanel);
    }


    // Inner class
    class Dish {
        private final String name;
        private final double price;
        private final JButton addItemButton;
        private final JButton subtractItemButton;
        private int count;

        public Dish(String name, double price, int count) {
            this.name = name;
            this.price = price;
            this.count = count;
            this.addItemButton = new JButton("Add");
            addItemButton.addActionListener(new AddItemListener());
            this.subtractItemButton = new JButton("Subtract");
            subtractItemButton.addActionListener(new SubtractItemListener());
        }

        public JButton getAddItemButton() {
            return addItemButton;
        }

        public JButton getSubtractItemButton() {
            return subtractItemButton;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }


        class AddItemListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent event) {
                setCount(getCount() + 1);
                setTotalSalesTax(getTotalSalesTax() + getPrice() * SALES_TAX);
                setTotalBeforeTax(getTotalBeforeTax() + getPrice());
                updateBill();
            }
        }

        class SubtractItemListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (getCount() > 0) {
                    setCount(Math.max(getCount() - 1, 0));
                    setTotalSalesTax(getTotalSalesTax() - getPrice() * SALES_TAX);
                    setTotalBeforeTax(getTotalBeforeTax() - getPrice());
                    updateBill();
                }
            }
        }
    }
}
