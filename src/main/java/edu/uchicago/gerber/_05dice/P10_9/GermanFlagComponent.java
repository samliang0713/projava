package edu.uchicago.gerber._05dice.P10_9;

import java.awt.*;

public class GermanFlagComponent extends FlagComponent {

    public GermanFlagComponent() {
        super(Color.BLACK, Color.RED, Color.YELLOW);
    }
}
