package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;

public class GermanFlagViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("German Flag");
        frame.setSize(300,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent germanFlag = new GermanFlagComponent();
        frame.add(germanFlag);
        frame.setVisible(true);
    }
}
