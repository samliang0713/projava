package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;
import java.awt.*;

public class FlagComponent extends JComponent {
    private Color color1;
    private Color color2;
    private Color color3;

    public FlagComponent(Color color1, Color color2, Color color3) {
        this.color1 = color1;
        this.color2 = color2;
        this.color3 = color3;
    }

    public void drawFlag(Graphics g, int xLeft, int yTop, int height) {
        g.setColor(this.color1);
        g.fillRect(xLeft, yTop+height, height*3*3/2, height);
        g.setColor(this.color2);
        g.fillRect(xLeft, yTop+height*2, height*3*3/2, height);
        g.setColor(this.color3);
        g.fillRect(xLeft, yTop+height*3, height*3*3/2, height);
    }


    @Override
    public  void paintComponent(Graphics g){
        drawFlag(g, 83, 100, 30);
    }
}
