package edu.uchicago.gerber._05dice.P10_9;

import java.awt.*;

public class HungarianFlag extends FlagComponent{
    public HungarianFlag() {
        super(Color.RED, Color.WHITE, new Color(52,124,44));
    }
}
