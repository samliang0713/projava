package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;

public class HungarianFlagViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Hungarian Flag");
        frame.setSize(300,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent hungarianFlag = new HungarianFlag();
        frame.add(hungarianFlag);
        frame.setVisible(true);
    }
}
