package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;

public class OlympicRingsViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Olympic Rings");
        frame.setSize(300,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent olympicRings = new OlympicRings();
        frame.add(olympicRings);
        frame.setVisible(true);
    }
}
