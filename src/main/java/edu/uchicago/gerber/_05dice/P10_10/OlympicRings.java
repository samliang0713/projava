package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;
import java.awt.*;

public class OlympicRings extends JComponent {


    public void drawRings(Graphics g, int xLeft, int yTop, int radius) {


        g.setColor(Color.blue);
        g.drawOval(xLeft + radius, yTop, radius, radius);


        g.setColor(Color.black);
        g.drawOval(xLeft + 2 * radius, yTop, radius, radius);


        g.setColor(Color.red);
        g.drawOval(xLeft + 3 * radius, yTop, radius, radius);


        g.setColor(Color.yellow);
        g.drawOval((int) (xLeft + (1.5 * radius)), (int) (yTop + (0.5 * radius)), radius, radius);


        g.setColor(Color.green);
        g.drawOval((int) (xLeft + (2.5 * radius)), (int) (yTop + (0.5 * radius)), radius, radius);


    }


    @Override
    public void paintComponent(Graphics g) {
    drawRings(g, 10,20,50);

    }
}
