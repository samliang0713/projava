package edu.uchicago.gerber._06design.R12_11;

public class Record {
    String countryName;
    double area;
    double population;

    public Record(String countryName, double area, double population) {
        this.countryName = countryName;
        this.area = area;
        this.population = population;
    }

    /**
     * Return country name
     * @return country name
     */
    public String getCountryName() {
        return countryName;
    }
      /**
       * Return country area
       * @return country area
       */
    public double getArea() {
        return area;
    }

    /**
     * Return country population
     * @return country population
     */
    public double getPopulation() {
        return population;
    }
}
