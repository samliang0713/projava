package edu.uchicago.gerber._06design.R12_11;

import java.util.List;

public class File {
    List<Record> countryList;


    public File(List<Record> countryList) {
        this.countryList = countryList;
    }

    /**
     * Return country with the largest area
     * @return country with the largest area
     */
    public String getCountryWithLargestArea() {
        return "Dummy Country";
    }

    /**
     * Return country with the largest population
     * @return country with the largest population
     */
    public String getCountryWithLargestPopulation() {
        return "Dummy Country";
    }

    /**
     * Return country with the largest population density
     * @return country with the largest population density
     */
    public String getCountryWithLargestDensity() {
        return "Dummy Country";
    }


}
