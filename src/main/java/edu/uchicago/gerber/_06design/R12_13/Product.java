package edu.uchicago.gerber._06design.R12_13;

public class Product {
    String name;
    double price;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
