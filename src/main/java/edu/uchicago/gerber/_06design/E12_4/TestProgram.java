package edu.uchicago.gerber._06design.E12_4;

public class TestProgram {
    private int score;

    public TestProgram() {
        this.score = 0;
    }

    public ArithmeticQuestion generateLevel1Question() {
        int input1 = (int) (Math.random()*8+1);
        int complement = (int) (Math.random() * (9 - input1) + 1);
        return new ArithmeticQuestion(input1, complement, '+');
    }

    public ArithmeticQuestion generateLevel2Question() {
        int input1 = (int) (Math.random()*9+1);
        int input2 = (int) (Math.random()*9+1);
        return new ArithmeticQuestion(input1, input2, '+');
    }

    public ArithmeticQuestion generateLevel3Question() {
        int input1 = (int) (Math.random()*9+1);
        int input2 = (int) (Math.random()*input1+1);
        return new ArithmeticQuestion(input1, input2, '-');
    }

    public void runProgram() {
        while(score < 15) {
            ArithmeticQuestion question;
            if(score < 5) {
                question = generateLevel1Question();
            }
            else if(score < 10) {
                question = generateLevel2Question();
            }
            else question = generateLevel3Question();

            if (question.askQuestion() == true) {
                score++;
                System.out.println("You now have a score of " + score);
            }
            else return;
        }
        System.out.println("Congrats, You have a score of 15!");
    }
}
