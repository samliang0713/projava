package edu.uchicago.gerber._06design.E12_4;

import java.util.Scanner;

public class ArithmeticQuestion {
    private int input1;
    private int input2;
    private char operation;
    private int answer;
    private int tryCount;

    public ArithmeticQuestion(int input1, int input2, char operation) {
        this.input1 = input1;
        this.input2 = input2;
        this.operation = operation;
        if(operation == '+') this.answer = input1 + input2;
        else if(operation == '-') this.answer = input1 - input2;
        tryCount = 0;
    }

    // Return true if user answers correctly, false otherwise.
    public boolean askQuestion() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is " + input1 + " " + operation + " " + input2 + "?");
        while(tryCount < 2) {
            int userAnswer = scanner.nextInt();
            if (userAnswer == answer) {
                System.out.println("Great! That is correct!");
                return true;
            }
            else {
                tryCount++;
                if(tryCount<2) System.out.println("Not Quite, try again.");
            }
        }
        System.out.println("You have tried 2 times");
        return false;
    }
}
