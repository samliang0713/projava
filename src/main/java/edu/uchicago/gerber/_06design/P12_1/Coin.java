package edu.uchicago.gerber._06design.P12_1;

public class Coin {
    private double value;

    public Coin(String str) {
        switch (str) {
            case "Q": value = 0.25;
                break;
            case "D": value = 0.1;
                break;
            case "N": value = 0.05;
                break;
            default: value = 0.0;
                break;
        }
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
