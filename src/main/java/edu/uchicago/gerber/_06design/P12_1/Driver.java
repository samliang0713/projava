package edu.uchicago.gerber._06design.P12_1;

import java.util.Scanner;

public class Driver {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        VendingMachine vendingMachine = new VendingMachine();
        vendingMachine.addProduct(new Product("Fuji Water", 3.00));
        vendingMachine.addProduct(new Product("Starbucks Iced Coffee", 5.00));
        vendingMachine.addProduct(new Product("Diet Coke", 1.50));
        vendingMachine.addProduct(new Product("Sprite", 1.50));

        vendingMachine.restock();

        outer:
        while(true) {
            System.out.println("Type 'i' to insert coins or 's' to select product or 'r' to return coins or 'exit' to quit");
            String strInitial = scanner.nextLine().toUpperCase();
            String strCoins, selectedProductName;
            switch (strInitial) {
                case "I":
                    strCoins = promptInsertCoins();
                    vendingMachine.insertCoins(strCoins);
                    System.out.println("Inserted Value: " + vendingMachine.getCurrentCredit());
                    break;
                case "S":
                    vendingMachine.showSelection();
                    selectedProductName = promptSelectProduct();
                    if(selectedProductName.equals( "OPERATOR")) {
                        vendingMachine.emptyBank();
                        break;
                    }
                    vendingMachine.purchaseProduct(selectedProductName);
                    break;
                case "R":
                    vendingMachine.returnCoins();
                    break;
                case "EXIT":
                    break outer;
            }

        }
        System.out.println("Thank you and have a nice day!");
    }

    private static String promptInsertCoins() {
        System.out.println("Insert coins in format: q q d m n");
        return scanner.nextLine().toUpperCase();
    }

    private static String promptSelectProduct() {
        System.out.println("Type which product you want to purchase");
        return scanner.nextLine().toUpperCase();
    }
}
