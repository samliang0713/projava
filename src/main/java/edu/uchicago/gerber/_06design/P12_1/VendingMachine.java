package edu.uchicago.gerber._06design.P12_1;

import java.util.*;

public class VendingMachine {
    private List<Coin> bank;
    private Map<Product, Integer> productCountMap;
    private List<Coin> currentCoins;
    private static int SLOTS = 10;

    public VendingMachine() {
        this.bank = new LinkedList<>();
        this.productCountMap = new HashMap<>();
        this.currentCoins = new LinkedList<>();
    }

    // Purchase and dispense a product based on the product name.
    public void purchaseProduct(String productName) {
        Product targetProduct = checkProduct(productName);
        if(targetProduct== null) {
            System.out.println("This machine does not sell this product");
            returnCoins();
            return;
        } else if(targetProduct.getPrice() > getCurrentCredit()) {
            System.out.println("Insufficient Fund");
            returnCoins();
            return;
        } else if(productCountMap.get(targetProduct) <= 0) {
            System.out.println("Item is sold out");
            returnCoins();
            return;
        }

        productCountMap.put(targetProduct, productCountMap.get(targetProduct) - 1);
        System.out.println("Enjoy your " + targetProduct.getName() + "!");
        for(Coin coin : currentCoins) {
            bank.add(coin);
            currentCoins = new LinkedList<>();
        }
    }

    // Check if the machine contains a product based on a string name
    private Product checkProduct(String productName) {
        for(Product product : productCountMap.keySet()) {
            if(product.getName().toUpperCase().equals(productName.toUpperCase())) return product;
        }
        return null;
    }

    // Insert coins
    public void insertCoins(List<Coin> coins) {
        for(Coin coin : coins) {
            currentCoins.add(coin);
        }
        System.out.println("You have inserted: " + getCurrentCredit());
    }
    public void insertCoins(String coins) {
        String[] coinsArray = coins.split(" ");
        for(String coin : coinsArray) {
            currentCoins.add(new Coin(coin));
        }
    }


    // Return coins
    public List<Coin> returnCoins() {
        List<Coin> returnCoins = currentCoins;
        currentCoins = new LinkedList<>();
        System.out.println("--- Returning your coins");
        return returnCoins;
    }

    // Add a new product
    public void addProduct(Product product) {
        if(!productCountMap.containsKey(product)) productCountMap.put(product, 0);
    }

    public double getCurrentCredit() {
        double totalCredit = 0;
        for(Coin coin : currentCoins) {
            totalCredit += coin.getValue();
        }
        return totalCredit;
    }

    public double getBankValue() {
        double totalCredit = 0;
        for(Coin coin : bank) {
            totalCredit += coin.getValue();
        }
        return totalCredit;
    }

    public void showSelection() {
        for(Product product : productCountMap.keySet()) {
            String productInfo = "- " + product.getName() + " / $" + product.getPrice() + " - ";
            if (productCountMap.get(product) == 0) productInfo = productInfo + "Sold Out";
            else productInfo = productInfo + productCountMap.get(product);
            System.out.println(productInfo);
        }
        System.out.println("Current Balance: " + getCurrentCredit());
    }



    // Restock every product
    public void restock() {
        for(Product product : productCountMap.keySet()) {
            productCountMap.put(product,SLOTS);
        }
    }

    public List<Coin> emptyBank() {
        System.out.println("Clearing the bank. Total value: " + getBankValue());
        List<Coin> checkOutCoins = bank;
        bank = new LinkedList<>();
        return checkOutCoins;
    }
}
