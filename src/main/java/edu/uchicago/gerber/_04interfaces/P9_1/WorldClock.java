package edu.uchicago.gerber._04interfaces.P9_1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorldClock extends Clock{
    private int offset;
    public WorldClock(int offset) {
        this.offset = offset;
    }

    @Override
    //Override the getHours method
    public String getHours() {
        Pattern pattern = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
        Matcher matcher = pattern.matcher(new java.util.Date().toString());


        if(matcher.find()){
            int hour = Integer.parseInt(matcher.group(0).substring(0,2));
            return "" + ((hour + this.offset) %24);
        }
        return "no match found";
    }
}
