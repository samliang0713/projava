package edu.uchicago.gerber._04interfaces.P9_1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Clock {

    public String getHours() {

    Pattern pattern = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
    Matcher matcher = pattern.matcher(new java.util.Date().toString());


    if(matcher.find()){
        return matcher.group(0).substring(0,2);
    }
    return "no match found";
    }

    public String getMinutes() {

        Pattern pattern = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
        Matcher matcher = pattern.matcher(new java.util.Date().toString());


        if(matcher.find()){
            return matcher.group(0).substring(3,5);
        }
        return "no match found";
    }

    public String getTime() {
        return "" + getHours() + ":" + getMinutes();
    }

}
