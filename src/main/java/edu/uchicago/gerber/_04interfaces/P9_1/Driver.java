package edu.uchicago.gerber._04interfaces.P9_1;

public class Driver {
    public static void main(String[] args) {
        Clock clock = new Clock();
        WorldClock worldClock = new WorldClock(9);
        System.out.println(clock.getHours());
        System.out.println(clock.getMinutes());
        System.out.println(clock.getTime());

        System.out.println(worldClock.getHours());
        System.out.println(worldClock.getTime());
    }
}
