#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################


R9.1 Superclas and subclass
    a. Employee (Superclass) / Manager (Subclass)
    b. Student (Superclass) / GraduateStudent (Subclass)
    c. Person (Superclass) / Student(Subclass)
    d. Employee (Superclass) / Professor(Subclass)
    e. BankAccount(Superclass) / CheckingAccount(Subclass)
    f. Vehicle (Superclass) / Car (Subclass)
    g. Vehicle (Superclass) / Minivan (Subclass)
    h. Car (Superclass) / Minivan (Subclass)
    i. Vehicle (Superclass) / Truck (Subclass)


R9.2 superclass and subclass
    1. It is not useful to have subclasses Toaster, CarVacuum and such because they don't have extra utilities or functions
    compared with the SmallAppliance superclass. We only care about the inventory here.

R9.4 SavingsAccount
    The SavingAccount class inherits the getBalance and deposit methods, and overrides the withdraw and monthEnd methods.
    It adds the setInterestRate method.

R9.6 Sandwich
    a.: legal
    b.: illegal
    c.: illegal
    d.: legal

R9.7 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
    (Classes are in the R9_7 package, where the Object class is the superclass for Person and Classroom

R9.8 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
    (Classes are in the R9_8 package)

R9.9 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
    (Classes are in the R9_9 package)

R9.10 Casting
    Casting from a primitive value means irreversible changes to the value. For example, casting a double variable of value 1.1 to an integer
    variable myInt means that the myInt has a value different from the original double value.

    However, casting for objects means we are widening the angle we are looking at the object, and we expect the underlying object
    can perform the tasks related to the class we are casting the object to. Sometimes it might produce an error because the underlying
    object does not belong to the class cast to. So it is best to run a check first.

R9.11 instanceof operator
    a. True
    b. True
    c. False
    d. True
    e. True
    f. False

R9.14 Edible interface
    a. Legal
    b. Illegal
    c. Illegal
    d. Legal
    e. Illegal
    f. Illegal
    g. Illegal
    h. Illegal