package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCan implements Measurable{
    private int height;
    private int radius;

    public SodaCan(int radius, int height) {
        this.height = height;
        this.radius = radius;
    }

    @Override
    public double getMeasure() {
        return getSurfaceArea();
    }

    public double getVolume() {
        return radius * radius * Math.PI * height;
    }

    public double getSurfaceArea() {
        return radius * radius * Math.PI * 2 + height * radius * 2 * Math.PI;
    }
}
