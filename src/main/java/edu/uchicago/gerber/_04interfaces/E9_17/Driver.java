package edu.uchicago.gerber._04interfaces.E9_17;

public class Driver {
    public static void main(String[] args) {
        SodaCan canA = new SodaCan(1,2);
        SodaCan canB = new SodaCan(3,4);
        SodaCan canC = new SodaCan(8,10);

        Measurable[] cans = new Measurable[]{canA, canB, canC};
        double sumSurfaceArea = 0;

        for(Measurable can : cans) {
            sumSurfaceArea+= can.getMeasure();
        }

        System.out.println("Average surface area is :" + sumSurfaceArea / cans.length);
    }
}
