package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    private String name;
    private int birthYear;

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String toString() {
        return "A person named " + this.name + " born in " + this.birthYear;
    }
}
