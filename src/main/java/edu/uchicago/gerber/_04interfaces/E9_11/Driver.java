package edu.uchicago.gerber._04interfaces.E9_11;

public class Driver {
    public static void main(String[] args) {
        Person joe  = new Person("Joe", 1937);
        Student potter = new Student("Potter", 1988, "Magic");
        Instructor dumbledore = new Instructor("Dumbledore", 1954, 45000);

        System.out.println(joe);
        System.out.println(potter);
        System.out.println(dumbledore);
    }
}
