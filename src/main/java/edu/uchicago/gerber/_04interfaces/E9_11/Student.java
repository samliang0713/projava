package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person{

    private String major;

    public Student(String name, int birthYear, String major) {
        super(name, birthYear);
        this.major = major;
    }

    public String toString() {
        return "A student named " + this.getName() + " born in " + this.getBirthYear() + " with a major of " + this.major;
    }
}
