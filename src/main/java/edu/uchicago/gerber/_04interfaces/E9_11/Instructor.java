package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person{
    private int salary;

    public Instructor(String name, int birthYear, int salary) {
        super(name, birthYear);
        this.salary = salary;
    }

    public String toString() {
        return "An Instructor named " + this.getName() + " born in " + this.getBirthYear() + " with a salary of " + this.salary;
    }
}
