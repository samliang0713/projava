package edu.uchicago.gerber._04interfaces.E9_13;

public class Driver {
    public static void main(String[] args) {
        BetterRectangle betterRectangle = new BetterRectangle(1,1,2,3);
        System.out.println(betterRectangle.getPerimeter());
        System.out.println(betterRectangle.getArea());
    }
}
