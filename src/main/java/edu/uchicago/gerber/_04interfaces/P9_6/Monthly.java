package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointment {
    public Monthly(String description, int year, int month, int day) {
        super(description, year, month, day);
    }


    // checks whether the date is after the first monthly appointment
    @Override
    boolean occursOn(int year, int month, int day) {
        return this.day == day && (this.year < year || (this.year == year  && this.month <= month));
    }

    @Override
    String appointmentType() {
        return "Monthly";
    }
}
