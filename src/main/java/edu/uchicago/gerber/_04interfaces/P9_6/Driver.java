package edu.uchicago.gerber._04interfaces.P9_6;
import java.util.*;
public class Driver {
    public static void main(String[] args) {

        Onetime bodyCheck = new Onetime("Body Check",2021,10,22);
        Daily gym = new Daily("Gym", 2020, 1, 1);
        Monthly dateNight = new Monthly("Date night", 2020,9,30);
        Daily study = new Daily("Study",2020,5,1);
        Monthly haircut = new Monthly("Haircut", 2019,7,13);

        Appointment[] appointments = new Appointment[]{bodyCheck, gym, dateNight, study, haircut};

        Scanner scanner = new Scanner(System.in);


        boolean yearInputExists = false;
        int yearInput = -1;

        //Prompt year
        while (!yearInputExists) {
            try {
                System.out.print("Please enter a year: ");
                yearInput = scanner.nextInt();
                if (yearInput > 0) yearInputExists = true;
                else System.out.println("Please enter a valid year");
            } catch (Exception e) {
                System.out.println("Please enter an integer");
                scanner.next();
            }
        }

        boolean monthInputExists = false;
        int monthInput = -1;

        //Prompt month
        while (!monthInputExists) {
            try {
                System.out.print("Please enter a month: ");
                monthInput = scanner.nextInt();
                if (monthInput > 0 && monthInput < 13) monthInputExists = true;
                else System.out.println("Please enter a valid month");
            } catch (Exception e) {
                System.out.println("Please enter an integer");
                scanner.next();
            }
        }

        boolean dayInputExists = false;
        int dayInput = -1;

        //Prompt month
        while (!dayInputExists) {
            try {
                System.out.print("Please enter a day: ");
                dayInput = scanner.nextInt();
                if (dayInput > 0 && dayInput < 32) dayInputExists = true;
                else System.out.println("Please enter a valid day");
            } catch (Exception e) {
                System.out.println("Please enter an integer");
                scanner.next();
            }
        }


        System.out.println("Appointments on "+monthInput + '/' + dayInput +'/' + yearInput);
        for (Appointment appointment : appointments) {
            if(appointment.occursOn(yearInput, monthInput, dayInput)) {
                System.out.println("" + appointment.appointmentType() + " : " + appointment.getDescription());
            }
        }
    }
}
