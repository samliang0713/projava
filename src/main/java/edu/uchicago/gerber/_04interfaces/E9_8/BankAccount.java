package edu.uchicago.gerber._04interfaces.E9_8;

public class BankAccount {
    private double balance;
    private static double PENALTY = 25;

    public BankAccount(double balance) {
        this.balance = balance;
    }

    public void withdraw(double amount) {

    }

    public double getBalance() {
        return balance;
    }

    public double getPENALTY() {
        return PENALTY;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
