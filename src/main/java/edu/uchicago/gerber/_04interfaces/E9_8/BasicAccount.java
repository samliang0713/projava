package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{

    public BasicAccount(double balance) {
        super(balance);
    }
    @Override
    public void withdraw(double amount) {
        // Doesn't allow withdrawal for more than balance
        if(amount < getBalance()) {
            System.out.println("Success!");
            setBalance(getBalance() - amount);
        }
        else {
            System.out.println("Insufficient fund");
        }
    }
}
