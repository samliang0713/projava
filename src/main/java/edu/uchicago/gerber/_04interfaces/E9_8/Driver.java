package edu.uchicago.gerber._04interfaces.E9_8;

public class Driver {
    public static void main(String[] args) {
        BankAccount account = new BasicAccount(200);
        System.out.println(account.getBalance());
        account.withdraw(199);
        account.withdraw(20);
    }
}
