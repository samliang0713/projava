package edu.uchicago.gerber._07streams.E19_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TransformString {
    public static void printTransformedWords(List<String> words) {
        Stream<String> stream = words.stream();
        stream.filter(c -> c.length()>2).forEach(c -> System.out.println(c.charAt(0) + "..." + c.charAt(c.length()-1)));
    }
    public static void main(String[] args) {
        List<String> words = new ArrayList<>(Arrays.asList(new String[]{"apple","pie","you"}));
        printTransformedWords(words);
    }
}
