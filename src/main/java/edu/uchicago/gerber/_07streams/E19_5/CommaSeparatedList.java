package edu.uchicago.gerber._07streams.E19_5;


import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class CommaSeparatedList {
    public static <T> String toString(Stream<T> stream, int n) {
        List<String> stringList = new LinkedList<>();
        stream.limit(n).forEach(c -> stringList.add("" + c));
        return stringList.toString();
    }

    public static void main(String[] args) {
        Stream<Double> doubleStream = Stream.of(0.1,0.5,0.77,0.98,0.6);
        System.out.println(toString(doubleStream, 3));
    }
}
