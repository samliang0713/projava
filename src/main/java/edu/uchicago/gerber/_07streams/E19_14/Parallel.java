package edu.uchicago.gerber._07streams.E19_14;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

public class Parallel {
    private static boolean isPalindrome(String str) {
        if(str.length() <= 1) return true;
        return str.substring(0,1).toUpperCase(Locale.ROOT).equals( str.substring(str.length()-1).toUpperCase(Locale.ROOT)) && isPalindrome(str.substring(1,str.length()-1));
    }
    public static void main(String[] args) throws IOException {
        String currentPath = new java.io.File(".").getCanonicalPath();
        System.out.println("Current dir:" + currentPath);


        // Import Dictionary
        Scanner dictScanner = new Scanner(new File(currentPath+ "/_07streams/E19_14/words.txt"));
        Set<String> dictionary = new LinkedHashSet();
        String wordInDict;
        while(dictScanner.hasNext()) {
            wordInDict= dictScanner.next();
            dictionary.add(wordInDict.toUpperCase(Locale.ROOT));
        }
        System.out.println("Dictionary size: " + dictionary.size());


        // Import War And Peace
        Scanner text = new Scanner(new File(currentPath+"/_07streams/E19_14/warAndPeace.txt"));
        List<String> wordList = new ArrayList<>();
        String word;
        while(text.hasNext()) {
            word = text.next();
            wordList.add(word.replaceAll("^[^a-zA-Z]+", "").replaceAll("[^a-zA-Z]+$", ""));
        }
        System.out.println("Text size: " + wordList.size());
        Stream<String> textStream = wordList.stream();

        String result = textStream.parallel().filter(s -> s.length()>=5)
                .filter(s -> isPalindrome(s)).findAny().orElse("");
        System.out.println("Any palindrome: " + result);
        System.out.println("*The output might be different every time you run the program.");


//        textStream.filter(s -> s.length()>=5 && isPalindrome(s)).forEach(v -> System.out.println(v));
//        long count = textStream.filter(s -> s.length()>=5 && isPalindrome(s)).count();
//        System.out.println(count);

    }
}
