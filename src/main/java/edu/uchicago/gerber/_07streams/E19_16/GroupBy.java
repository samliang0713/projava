package edu.uchicago.gerber._07streams.E19_16;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupBy {
    public static void main(String[] args) throws IOException {
        String currentPath = new File(".").getCanonicalPath();
        System.out.println("Current dir:" + currentPath);

        BufferedReader br = new BufferedReader(new FileReader(currentPath + "/_07streams/E19_16/wordsInput.txt"));

        Map<Character, Double> avgLength  = br.lines().collect(Collectors.groupingBy(w -> w.charAt(0), Collectors.averagingLong(w -> w.length())));

        System.out.println(avgLength.toString());
    }
}
