package edu.uchicago.gerber._07streams.R19_1;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class STREAM {
    public static void main(String[] args) {
        DoubleStream doubleStream1 = DoubleStream.of(10.2, 11.8, 33.2);
//        Stream<>

        Stream<String> stringStream = Stream.of("10.32","11.21","33.21");
        List<Double> doubleList = stringStream.map(c -> Double.parseDouble(c)).collect(Collectors.toList());
        Stream<Double> doubleStream = Stream.of(new Double(10.2),new Double(12.2),new Double(13.2));
        double a = 2.2;
        List<String> stringList = doubleStream.map(c -> "d:" + c).collect(Collectors.toList());
        System.out.println(stringList.toString());
        System.out.println(doubleList.toString());
        LinkedList b = new LinkedList();

    }
}
