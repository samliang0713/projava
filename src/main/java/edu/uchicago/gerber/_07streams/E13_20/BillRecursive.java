package edu.uchicago.gerber._07streams.E13_20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BillRecursive {

    private static void printWays(List<ArrayList<Integer>> ways) {
        System.out.println("Total number of ways: " + ways.size());
        System.out.println("The ways are: [hundreds, twenties, fives, ones]");
        for(ArrayList<Integer> array: ways) {
            System.out.println("[ " + array.get(0) + " , " +  array.get(1) + " , " +  array.get(2) + " , "+  array.get(3) + " ]");
        }
    }

    private static List<ArrayList<Integer>> calculateWays(int price) {
        // stores the output
        List<ArrayList<Integer>> wayList = new LinkedList<>();

        // stores the intermediate step
        List<ArrayList<Integer>> visitedCombo = new LinkedList<>();
        billRecursiveHelper(wayList, visitedCombo, price, 0,0,0,0);
        return wayList;
    }

    private static void billRecursiveHelper(List<ArrayList<Integer>> wayList, List<ArrayList<Integer>> visitedCombo, int price, int hundreds, int twenties, int fives, int ones) {

        if(price < 0) return;
        if(price == 0 && !visitedCombo.contains(new ArrayList<>(Arrays.asList(price,hundreds, twenties, fives, ones)))) {
            wayList.add(new ArrayList<>(Arrays.asList(hundreds, twenties, fives, ones)));
            visitedCombo.add(new ArrayList<>(Arrays.asList(price,hundreds, twenties, fives, ones)));
            return;
        }

        if(!visitedCombo.contains(new ArrayList<>(Arrays.asList(price,hundreds, twenties, fives, ones)))) {
            billRecursiveHelper(wayList, visitedCombo, price - 100, hundreds + 1, twenties, fives, ones);
            billRecursiveHelper(wayList, visitedCombo, price - 20, hundreds, twenties + 1, fives, ones);
            billRecursiveHelper(wayList, visitedCombo, price - 5, hundreds, twenties, fives + 1, ones);
            billRecursiveHelper(wayList, visitedCombo, price - 1, hundreds, twenties, fives, ones + 1);
        }

        visitedCombo.add(new ArrayList<>(Arrays.asList(price,hundreds, twenties, fives, ones)));
    }



    public static void main(String[] args) {

        List<ArrayList<Integer>> ways = calculateWays(25);

        printWays(ways);

    }
}
