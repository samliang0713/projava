package edu.uchicago.gerber._07streams.Yoda;

import java.util.Scanner;

public class YodaRecursive {
    private static String reverseSentenceRecursive(String sentence) {
        String[] splitSentence = sentence.split(" ");
         return reverseSentenceRecursiveHelper(sentence.split(" "), splitSentence.length-1);
    }

    private static String reverseSentenceRecursiveHelper(String[] splitSentence, int i) {
        if(i == 0 ) return splitSentence[0];
        return splitSentence[i] +" " + reverseSentenceRecursiveHelper(splitSentence, i-1);
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please say a few words to Recursive Yoda");
        String words = scanner.nextLine();
        System.out.println(reverseSentenceRecursive(words));
    }
}
