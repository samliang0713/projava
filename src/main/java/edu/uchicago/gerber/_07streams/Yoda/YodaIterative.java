package edu.uchicago.gerber._07streams.Yoda;

import java.util.Scanner;

public class YodaIterative {

    private  static String reverseSentenceIterative(String sentence) {
        String[] splitSentence = sentence.split(" ");
        String result = "";
        for(int i = splitSentence.length-1; i >=0 ; i--) {
            result = result + " " + splitSentence[i];
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please say a few words to Yoda");
        String words = scanner.nextLine();

        System.out.println(reverseSentenceIterative(words));
    }
}
