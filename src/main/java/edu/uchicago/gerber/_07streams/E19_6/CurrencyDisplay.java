package edu.uchicago.gerber._07streams.E19_6;

import java.util.Currency;
import java.util.Set;
import java.util.stream.Stream;

public class CurrencyDisplay {
    public static void main(String[] args) {
        Set<Currency> currencySet = Currency.getAvailableCurrencies();
        // Turn into a stream of currency
        Stream<Currency> currencyStream = currencySet.stream();

        // Turn into a stream of name
        Stream<String> nameStream = currencyStream.map(c -> c.getDisplayName());

        // Sort and print
        nameStream.sorted((a,b) -> a.compareTo(b)).forEach(a -> System.out.println(a));

    }
}
