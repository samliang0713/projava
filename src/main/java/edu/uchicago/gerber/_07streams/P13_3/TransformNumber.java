package edu.uchicago.gerber._07streams.P13_3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class TransformNumber {


    public static List<String> numberToLetter(String number) {
        List<String> outputList = new LinkedList<>();
        String[] letterArray = new String[]{"", "","ABC","DEF","GHI","JKL","MNO","PQRS","TUV","WXYZ"};
        numberToLetterHelper(outputList,  number, "", letterArray);
        return outputList;
    }

    private static void numberToLetterHelper(List<String> outputList, String number, String prefix, String[] letterArray) {
        if (number.length() == 0) {
            outputList.add(prefix);
            return;
        }
        int firstNum = Integer.parseInt(number.substring(0,1));
        for(int i = 0; i < letterArray[firstNum].length(); i++) {
            numberToLetterHelper(outputList, number.substring(1),prefix+letterArray[firstNum].charAt(i),letterArray);
        }
    }

    private static List<String>  generateWordSequence( String number, Set<String> dictionary) {
        List<String> letterOutput = numberToLetter(number);
        List<String> wordSequence = new LinkedList<>();
        for(String output : letterOutput) {
            generateWordSequenceHelper(wordSequence,dictionary , output, "");
        }
         return wordSequence;
    }

    private static void generateWordSequenceHelper( List<String> wordSequence, Set<String> dictionary, String word, String prefix) {
        if(word.length() == 0 && !wordSequence.contains(prefix)) {
            wordSequence.add(prefix);
        }

        // Excluding the 1 letter word
        for(int i = 2; i <= word.length(); i++) {
            String substring = word.substring(0,i);
            if(dictionary.contains(substring)) {

                generateWordSequenceHelper(wordSequence, dictionary, word.substring(i), prefix + substring);
            }
        }
    }


    public static void main(String[] args) throws IOException {
        String currentPath = new java.io.File(".").getCanonicalPath();
        System.out.println("Current dir:" + currentPath);

        // Import Dictionary
        BufferedReader br = new BufferedReader(new FileReader(currentPath+ "/_07streams/P13_3/words.txt"));
        Set<String> dictionary = new LinkedHashSet();
        String line;
        while((line = br.readLine()) != null) dictionary.add(line.toUpperCase(Locale.ROOT));
        br.close();
        System.out.println(dictionary.size());

        // Transform numbers
        List<String> letterOutput = numberToLetter("2633465282");
//        System.out.println(letterOutput.size());
//        System.out.println(letterOutput);



        // Generate word sequence
        List<String> wordSequence = generateWordSequence("2633465282", dictionary);
        System.out.println(wordSequence.size());
        System.out.println(wordSequence);
    }
}
