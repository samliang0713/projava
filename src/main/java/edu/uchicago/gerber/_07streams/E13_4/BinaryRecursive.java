package edu.uchicago.gerber._07streams.E13_4;

public class BinaryRecursive {

    private static String binaryRecursive(int n) {
        if (n <= 1) return "" + n;
        return "" + binaryRecursive(n / 2) + n%2;
    }

    public static void main(String[] args) {
        System.out.println(binaryRecursive(2));
        System.out.println(binaryRecursive(9));
        System.out.println(binaryRecursive(42));
    }
}
