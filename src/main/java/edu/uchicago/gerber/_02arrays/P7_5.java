package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class P7_5 {

    public static void main(String[] args) throws FileNotFoundException {

        String csvFileName;
        Scanner fileNameScanner = new Scanner(System.in);
        System.out.println("Please type the file name, including suffix");
        csvFileName = fileNameScanner.next();
        fileNameScanner.close();
        File csvFile = new File(csvFileName);

        try {
            CSVReader reader = new CSVReader(csvFile);
            System.out.println(reader.numberOfRow());
            System.out.println(reader.numberOfFields(2));
            System.out.println(reader.field(2, 8));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
