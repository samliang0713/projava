package edu.uchicago.gerber._02arrays;

import java.util.Arrays;
import java.util.Random;

public class E6_12 {
    public static int[] generateRandomArray(int size) {
        // generate an array of length size with integers between 0 and 99 (inclusive)
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = generateRandomArray(20);
        System.out.println("Print the original array");
        for (int value : array) {
            System.out.print(value + " ");
        }

        Arrays.sort(array);
        System.out.println("\nPrint the sorted array");
        for (int value : array) {
            System.out.print(value + " ");
        }

    }
}
