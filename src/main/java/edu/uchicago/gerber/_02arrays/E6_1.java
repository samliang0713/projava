package edu.uchicago.gerber._02arrays;

import java.util.Random;

public class E6_1 {
    // Generate an array with 10 random arrays between 0 and 100 (exclusive);
    public static int[] randomArray() {
        Random random = new Random();
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static void main(String[] args) {

        int[] array = randomArray();

        System.out.println("Every element at an even index: ");
        for (int i = 0; i < array.length; i += 2) {
            System.out.print(array[i] + " ");
        }

        System.out.println("\nEvery even element: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) System.out.print(array[i] + " ");
        }

        System.out.println("\nAll elements in reverse order: ");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }

        System.out.println("\nOnly the first and last element: ");
        System.out.print(array[0] + " " + array[array.length - 1]);

    }
}
