package edu.uchicago.gerber._02arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class E6_16 {

    public static int findMax(ArrayList<Integer> intValues) {
        int max = intValues.get(0);
        for(Integer value : intValues) {
            max = Math.max(max, value);
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        ArrayList<Integer> intValues = new ArrayList<Integer>();

        while (true) {
            try {
                System.out.print("Type integer value or \"quit\" to exit:");
                //any value that is not an integer (including "quit") will throw an exception, which breaks out of the loop
                intValues.add(scan.nextInt());
            } catch (Exception e) {
                break;
            }
        }

        if (intValues.size() == 0) {
            System.out.println("Not enough data");
            return;
        }

        //Find the max value of the list
        int max = findMax(intValues);
        int NUM_ASTERISKS = 20;

        //Set each element to be 20 multiplied by the fraction of itself and the max element
        for(int i = 0; i < intValues.size(); i++) {
            intValues.set(i, NUM_ASTERISKS * intValues.get(i) / max);
        }

        for(int i = NUM_ASTERISKS; i >= 0; i--) {
            for(int j = 0; j < intValues.size(); j++ ) {
                if(intValues.get(j) > i) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.println();
        }
    }

}
