package edu.uchicago.gerber._02arrays;

import java.util.*;
import java.io.*;

public class E7_4 {
    public static void main(String[] args) throws FileNotFoundException {
        String inputFileName;
        String outputFileName;

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Please type the input file name, including suffix");
        inputFileName = inputScanner.next();
        System.out.println("Please type the output file name, including suffix");
        outputFileName = inputScanner.next();

        File inputFile = new File(inputFileName);
        PrintWriter outputFile = new PrintWriter(outputFileName);
        Scanner inputFileScanner = new Scanner(inputFile);

        int counter = 1;
        while(inputFileScanner.hasNextLine()) {
            String line = inputFileScanner.nextLine();
            outputFile.println("/* " + counter + " */" + line);
            counter++;
        }


        inputScanner.close();
        inputFileScanner.close();
        outputFile.close();
    }
}
