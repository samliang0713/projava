package edu.uchicago.gerber._02arrays;

public class P5_24 {
    // Only works up to 3999, based on description outlined in P3.13

    public static int romanStringToNumber(String romanString) {
        int total = 0;
        String str = romanString;
        while (str.length() > 0) {
            if (str.length() == 1 || romanSymbolToNumber(str.charAt(0)) >= romanSymbolToNumber(str.charAt(1))) {
                total += romanSymbolToNumber(str.charAt(0));
                str = str.substring(1);
            } else {
                int difference = romanSymbolToNumber(str.charAt(1)) - romanSymbolToNumber(str.charAt(0));
                total += difference;
                str = str.substring(2);
            }
        }
        return total;
    }

    public static int romanSymbolToNumber(char roman) {
        switch (roman) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
        }
        // shouldn't return 0 if the roman numerals are correct inputs
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(romanStringToNumber("MMDLVII"));
    }

}
