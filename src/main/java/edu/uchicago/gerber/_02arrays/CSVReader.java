package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.util.Scanner;

public class CSVReader {
    public File csvFile;

    public CSVReader(File csvFile) {
        this.csvFile = csvFile;
    }

    public int numberOfRow() {
        int numRow = 0;
        try (Scanner csvScanner = new Scanner(this.csvFile, "ISO-8859-1")) {
            while (csvScanner.hasNextLine()) {
                numRow++;
                csvScanner.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numRow;
    }

    public int numberOfFields(int row) {
        // return -1 if row number is larger than the row size of the csv file.
        if (row > this.numberOfRow()) return -1;
        int numFields = 0;
        try (Scanner csvScanner = new Scanner(csvFile, "ISO-8859-1")) {
            int rowCounter = 1;
            while (row > rowCounter) {
                rowCounter++;
                csvScanner.nextLine();
            }

            String[] parts = csvScanner.nextLine().split(",");
            int commaWithinField = 0;
            for (int i = 0; i < parts.length; i++) {
                if (parts[i].trim().charAt(0) == '"') {
                    while (parts[i].charAt(parts[i].length() - 1) != '"' && i < parts.length - 1) {
                        commaWithinField++;
                        i++;
                    }
                }
            }
            numFields = parts.length - commaWithinField;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Dummy response
        return numFields;

    }

    public String field(int row, int column) {
        if (row > this.numberOfRow()) return "Row num exceeds max row size";
        if (column > this.numberOfFields(row)) return "Col num exceeds max col size";
        String targetField = "";
        try (Scanner csvScanner = new Scanner(csvFile, "ISO-8859-1")) {
            int rowCounter = 1;
            while (row > rowCounter) {
                rowCounter++;
                csvScanner.nextLine();
            }
            String[] parts = csvScanner.nextLine().split(",");

            int fieldCounter = 1;
            int index = 0;
            while (column > fieldCounter) {
                if (parts[index].trim().charAt(0) == '"') {
                    while (parts[index].charAt(parts[index].length() - 1) != '"' && index < parts.length - 1) {
                        index++;
                    }
                }
                index++;
                fieldCounter++;
            }

            if (parts[index].trim().charAt(0) != '"') targetField = parts[index].trim();
            else {
                targetField = "";
                while (parts[index].charAt(parts[index].length() - 1) != '"' && index < parts.length - 1) {
                    targetField += parts[index] + ",";
                    index++;
                }
                targetField += parts[index];

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetField;
    }
}
