package edu.uchicago.gerber._02arrays;

public class P5_8 {

    public static boolean isLeapYear(int year) {
        // Check if 4 divides year. If not, it is not a leap year
        if (year % 4 != 0) return false;

        else {
            // If 4 divides the year, it is a leap year if it is either a multiple of 400 or it is not a multiple of 100
            return year % 100 != 0 || year % 400 == 0;
        }
    }


    public static void main(String[] args) {
        System.out.println(isLeapYear(2000));
        System.out.println(isLeapYear(1996));
        System.out.println(isLeapYear(1900));
    }


}
