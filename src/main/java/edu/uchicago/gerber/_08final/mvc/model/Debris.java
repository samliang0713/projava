package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.List;



public class Debris extends Sprite {
    public static final int MAX_DEBRIS = 30;
    public static final Color DEFAULT_COLOR = new Color(66,66,155);
    public Debris() {
        //call Sprite constructor
        super();

        // emerges from the edge of the screen
        int randomInt = Game.R.nextInt( 4);;
        double randomDouble = Game.R.nextDouble( );
        switch (randomInt) {
            case 0:
                // Left
                setCenter(new Point(15, (int) (Game.DIM.height*randomDouble)));
                setDeltaX(2+Game.R.nextInt(2));
                setDeltaY(randomDouble < 0.5 ? -3 : 3);
                break;
            case 1:
                // Right
                setCenter(new Point(Game.DIM.width-15, (int) (Game.DIM.height*randomDouble)));
                setDeltaX(-2-Game.R.nextInt(2));
                setDeltaY(randomDouble < 0.5 ? 3 : -3);
                break;
            case 2:
                // Top
                setCenter(new Point((int) (Game.DIM.width*randomDouble),15));
                setDeltaY(2+Game.R.nextInt(2));
                setDeltaX(randomDouble < 0.5 ? 3 : -3);
                break;
            case 3:
                // Bottom
                setCenter(new Point((int) (Game.DIM.width*randomDouble),Game.DIM.height-15));
                setDeltaY(-2-Game.R.nextInt(2));
                setDeltaX(randomDouble <  0.5 ? 3 : -3);
                break;
        }


        setRadius(10);
        setTeam(Team.DEBRIS);

        //the spin will be either plus or minus 0-5
        setSpin(somePosNegValue(6));

        //this method is in place of setting cartesian points
        assignRandomShape();

        setColor(DEFAULT_COLOR);
    }

    public Debris(Sprite origin) {
        this();
        setCenter(origin.getCenter());
        setColor(origin.getColor());
    }



    public void assignRandomShape (){


        //6.283 is the max radians
        final int MAX_RADIANS_X1000 =6283;

        int sides = Game.R.nextInt( 7 ) + 17;
        PolarPoint[] polPolars = new PolarPoint[sides];
        for ( int nC = 0; nC < polPolars.length; nC++ ){
            double r = (800 + Game.R.nextInt(200)) / 1000.0; //number between 0.8 and 1.0
            double theta = Game.R.nextInt(MAX_RADIANS_X1000) / 1000.0; // number between 0 and 6.283
            polPolars[nC] = new PolarPoint(r,theta);
        }

        setCartesians(polarToCartesian(
                Arrays.stream(polPolars)
                        .sorted(new Comparator<PolarPoint>() {
                            @Override
                            public int compare(PolarPoint p1, PolarPoint p2) {
                                return  p1.getTheta().compareTo(p2.getTheta());
                            }
                        })
                        .collect(Collectors.toList()))
        );


    }

    @Override
    public void move(){
        super.move();

        //a debris spins, so you need to adjust the orientation at each move()
        setOrientation(getOrientation() + getSpin());

        // remove asteroid from game if it reaches the bound of the frame
        double posX = getCenter().x;
        double posY = getCenter().y;

        if(posY <= 0 || posX <= 0 || posX >= Game.DIM.width || posY >= Game.DIM.height) {
        removeFromScreen();
        CommandCenter.getInstance().setNumDebris(CommandCenter.getInstance().getNumDebris()-1);
        }
    }


//    @Override
//    public void draw(Graphics g) {
//
//        g.setColor(getColor());
//        render(g);
//
//    }

    @Override
    public void render(Graphics g) {



        List<PolarPoint> polars = cartesianToPolar(Arrays.asList(getCartesians()));

        Function<PolarPoint,Point> adjustPointFunction =
                pp -> new Point(
                        (int) (getCenter().x + pp.getR() * getRadius()
                                * Math.sin(Math.toRadians(getOrientation())
                                + pp.getTheta())),

                        (int) (getCenter().y - pp.getR() * getRadius()
                                * Math.cos(Math.toRadians(getOrientation())
                                + pp.getTheta())));


        g.fillPolygon(
                polars.stream()
                        .map(adjustPointFunction)
                        .map(pnt -> pnt.x)
                        .mapToInt(Integer::intValue)
                        .toArray(),

                polars.stream()
                        .map(adjustPointFunction)
                        .map(pnt -> pnt.y)
                        .mapToInt(Integer::intValue)
                        .toArray(),

                getCartesians().length);


        //for debugging center-point. Feel free to remove these two lines.
        //#########################################
//        g.setColor(Color.ORANGE);
//        g.fillOval(getCenter().x -1, getCenter().y -1, 2,2);
        //g.drawOval(getCenter().x - getRadius(), getCenter().y - getRadius(), getRadius() *2, getRadius() *2);
        //#########################################
    }


}
