package edu.uchicago.gerber._08final.mvc.controller;


import javax.sound.sampled.*;
import java.io.IOException;

public class Sound {

	//for individual wav sounds (not looped)
	//http://stackoverflow.com/questions/26305/how-can-i-play-sound-in-java
	public static synchronized void playSound(final String strPath) {
	    new Thread(new Runnable() { 
	      public void run() {
	        try {
	          Clip clp = AudioSystem.getClip();

	          AudioInputStream aisStream = 
	        		  AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream("/sounds/" + strPath));
     
	          
	          clp.open(aisStream);
	          clp.start(); 
	        } catch (Exception e) {
	          System.err.println(e.getMessage());
	        }
	      }
	    }).start();
	  }
	
	
	//for looping wav clips
	//http://stackoverflow.com/questions/4875080/music-loop-in-java
	public static Clip clipForLoopFactory(String strPath){

		Clip clp = null;
		try {
			String currentPath = new java.io.File(".").getCanonicalPath();
			System.out.println(currentPath+ "/src/main/java/resources/sounds/"  + strPath);

			AudioInputStream aisStream =
					  AudioSystem.getAudioInputStream(Sound.class.getResourceAsStream("/sounds/"  + strPath));
			clp = AudioSystem.getClip();
		    clp.open( aisStream );

		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException exp) {
			exp.printStackTrace();
		} //the next three lines were added to catch all exceptions generated

		return clp;

	}
	
	


}
