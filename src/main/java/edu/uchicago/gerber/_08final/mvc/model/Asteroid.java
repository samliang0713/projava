package edu.uchicago.gerber._08final.mvc.model;


import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;


public class Asteroid extends Sprite {

	//radius of a large asteroid
	private final int LARGE_RADIUS = 60;
	private final int MEDIUM_RADIUS = 40;
	private final int SMALL_RADIUS = 20;
	public static final int MAX_ASTEROIDS = 8;
	
	//size determines if the Asteroid is Large (0), Medium (1), or Small (2)
	//when you explode a Large asteroid, you should spawn 2 or 3 medium asteroids
	//same for medium asteroid, you should spawn small asteroids
	//small asteroids get blasted into debris, but do not spawn anything
	public Asteroid(int size){
		
		//call Sprite constructor
		super();

		// emerges from the edge of the screen
		int randomInt = Game.R.nextInt( 4);;
		double randomDouble = Game.R.nextDouble( );
		switch (randomInt) {
			case 0:
				// Left
				setCenter(new Point(15, (int) (Game.DIM.height*randomDouble)));
				setDeltaX(2+Game.R.nextInt(2));
				setDeltaY(randomDouble < 0.5 ? -3 : 3);
				break;
			case 1:
				// Right
				setCenter(new Point(Game.DIM.width-15, (int) (Game.DIM.height*randomDouble)));
				setDeltaX(-2-Game.R.nextInt(2));
				setDeltaY(randomDouble < 0.5 ? 3 : -3);
				break;
			case 2:
				// Top
				setCenter(new Point((int) (Game.DIM.width*randomDouble),15));
				setDeltaY(2+Game.R.nextInt(2));
				setDeltaX(randomDouble < 0.5 ? 3 : -3);
				break;
			case 3:
				// Bottom
				setCenter(new Point((int) (Game.DIM.width*randomDouble),Game.DIM.height-15));
				setDeltaY(-2-Game.R.nextInt(2));
				setDeltaX(randomDouble <  0.5 ? 3 : -3);
				break;
		}


		setTeam(Team.FOE);

		//the spin will be either plus or minus 0-5
		setSpin(somePosNegValue(6));
		
		//an size of zero is a big asteroid
		//a size of 1 or 2 is med or small asteroid respectively
		if (size == 0) {
			setRadius(LARGE_RADIUS);
			setSpriteScore(20);
		}
		else if (size == 1) {
			setRadius(MEDIUM_RADIUS);
			setSpriteScore(40);
		}
		else {
			setRadius(SMALL_RADIUS);
			setSpriteScore(60);
		}

		//this method is in place of setting cartesian points
		assignRandomShape();

	}



	//overloaded so we can spawn smaller asteroids from an exploding one
	public Asteroid(Asteroid astExploded){

		//calls the other constructor: Asteroid(int size)
		this(astExploded.getSize() + 1);
		setCenter(astExploded.getCenter());
		int newSmallerSize = astExploded.getSize() + 1;
		//random delta-x - the smaller the asteroid the faster its possible speed
		setDeltaX(somePosNegValue(6 + newSmallerSize * 2));
		//random delta-y - the smaller the asteroid the faster its possible speed
		setDeltaY(somePosNegValue(6 + newSmallerSize * 2));

	}

	public int getSize(){

		switch (getRadius()) {
			case LARGE_RADIUS:
				return 0;
			case MEDIUM_RADIUS:
				return 1;
			case SMALL_RADIUS:
				return 2;
			default:
				return 0;
		}
	}


	@Override
	public void move(){
		super.move();
		
		//an asteroid spins, so you need to adjust the orientation at each move()
		setOrientation(getOrientation() + getSpin());

		// remove asteroid from game if it reaches the bound of the frame
		double posX = getCenter().x;
		double posY = getCenter().y;

		if(posY <= 0 || posX <= 0 || posX >= Game.DIM.width || posY >= Game.DIM.height) {
			if(getSize() == 0) CommandCenter.getInstance().setNumAsteroids(CommandCenter.getInstance().getNumAsteroids()-1);
			removeFromScreen();
		}
	}


	  public void assignRandomShape (){

		  //6.283 is the max radians
		  final int MAX_RADIANS_X1000 =6283;

		  int sides = Game.R.nextInt( 7 ) + 17;
		  PolarPoint[] polPolars = new PolarPoint[sides];
		  for ( int nC = 0; nC < polPolars.length; nC++ ){
			  double r = (800 + Game.R.nextInt(200)) / 1000.0; //number between 0.8 and 1.0
			  double theta = Game.R.nextInt(MAX_RADIANS_X1000) / 1000.0; // number between 0 and 6.283
			  polPolars[nC] = new PolarPoint(r,theta);
		  }

		 setCartesians(polarToCartesian(
				 Arrays.stream(polPolars)
				 .sorted(new Comparator<PolarPoint>() {
					 @Override
					 public int compare(PolarPoint p1, PolarPoint p2) {
						 return  p1.getTheta().compareTo(p2.getTheta());
					 }
				 })
				 .collect(Collectors.toList()))
		 );

	  }


}
