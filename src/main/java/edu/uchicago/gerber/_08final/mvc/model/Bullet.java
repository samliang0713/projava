package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Bullet extends Sprite {

    private final double FIRE_POWER = 35.0;


    public Bullet(Sprite origin) {

        super();
        if(origin instanceof Falcon) {
            setTeam(Team.FRIEND);
            setColor(Color.white);
        } else {
            setTeam(Team.FOE);
            setColor(Color.RED);
        }



        //a bullet expires after 20 frames
        setExpiry(20);
        setRadius(6);


        //everything is relative to the falcon ship that fired the bullet
        setDeltaX(origin.getDeltaX() +
                Math.cos(Math.toRadians(origin.getOrientation())) * FIRE_POWER);
        setDeltaY(origin.getDeltaY() +
                Math.sin(Math.toRadians(origin.getOrientation())) * FIRE_POWER);
        setCenter(origin.getCenter());

        //set the bullet orientation to the falcon (ship) orientation
        setOrientation(origin.getOrientation());

        //make sure to setCartesianPoints last
        //defined the points on a cartesian grid
        List<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(0, 3)); //top point

        pntCs.add(new Point(1, -1));
        pntCs.add(new Point(0, -2));
        pntCs.add(new Point(-1, -1));
        setCartesians(pntCs);


    }

    @Override
    public void move() {
        super.move();
        expire();


        // remove bullet  from game if it reaches the bound of the frame
        double posX = getCenter().x;
        double posY = getCenter().y;

        if(posY <= 0 || posX <= 0 || posX >= Game.DIM.width || posY >= Game.DIM.height) {
            removeFromScreen();
        }
    }

}
