package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;

public class Explosion extends Sprite  {
    private int radiusIncrement;
    public Explosion(Sprite origin) {
        super();
        setTeam(Team.DEBRIS);
        setDeltaX(0);
        setDeltaY(0);
        setCenter(origin.getCenter());
        setOrientation(origin.getOrientation());
        setRadius(10);
        setExpiry(5);
        setColor(new Color(255,165,0,255));
        setRadiusIncrement(1);
    }


    @Override
    public void draw(Graphics g) {

        int alpha = getColor().getAlpha();
        setColor(new Color(getColor().getRed(),getColor().getGreen(),getColor().getBlue(), Math.max(125,alpha-10)));
        g.setColor(getColor());
        g.fillOval(getCenter().x - getRadius(), getCenter().y - getRadius(), getRadius()*2, getRadius()*2);


        //for debugging center-point. Feel free to remove these two lines.
        //#########################################
//        g.setColor(Color.ORANGE);
//        g.fillOval(getCenter().x -1, getCenter().y -1, 2,2);
        //g.drawOval(getCenter().x - getRadius(), getCenter().y - getRadius(), getRadius() *2, getRadius() *2);
        //#########################################
    }

    @Override
    public void move() {
        super.move();
        expire();

        if(getExpiry() > 0) {
            setRadius(getRadius()+radiusIncrement);
        }

//        System.out.println("radius： " + getRadius());
    }

    @Override
    public boolean isProtected() {
        return getExpiry() > 0;
    }

    public int getRadiusIncrement() {
        return radiusIncrement;
    }

    public void setRadiusIncrement(int radiusIncrement) {
        this.radiusIncrement = radiusIncrement;
    }
}
