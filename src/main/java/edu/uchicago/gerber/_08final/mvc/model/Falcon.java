package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class Falcon extends Sprite {

	// ==============================================================
	// FIELDS 
	// ==============================================================
	
	private final double THRUST = .65;
	private final double BRAKE = 0.95;
	final int DEGREE_STEP = 9;
	
	//private boolean shield = false;
	private boolean thrusting = false;
	private boolean moveRight = false;
	private boolean moveLeft = false;
	private boolean moveUp = false;
	private boolean moveDown = false;
	private boolean tripleBullet = false;
	private int tripleBulletCount = 0;
	public static final int TRIPLE_BULLET_LIMIT = 60;
	public static int FADE_INITIAL_VALUE = 0;

	// ==============================================================
	// CONSTRUCTOR
	// ==============================================================

	public Falcon() {
		super();
		setTeam(Team.FRIEND);
		setColor(Color.green);
		
		//put falcon in the middle.
		setCenter(new Point(Game.DIM.width / 2, Game.DIM.height / 2));
		
		//pointing up
		setOrientation(270);
		
		//this is the size (radius) of the falcon
		setRadius(35);

		//Falcon uses fade.
		setFade(FADE_INITIAL_VALUE);

		setTripleBullet(false);

		//be sure to set cartesian points last.
		List<Point> pntCs = new ArrayList<>();
		// Robert Alef's awesome falcon design
		pntCs.add(new Point(0,9));
		pntCs.add(new Point(-1, 6));
		pntCs.add(new Point(-1,3));
		pntCs.add(new Point(-4, 1));
		pntCs.add(new Point(4,1));
		pntCs.add(new Point(-4,1));

		pntCs.add(new Point(-4, -2));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-4, -2));

		pntCs.add(new Point(-10, -8));
		pntCs.add(new Point(-5, -9));
		pntCs.add(new Point(-7, -11));
		pntCs.add(new Point(-4, -11));
		pntCs.add(new Point(-2, -9));
		pntCs.add(new Point(-2, -10));
		pntCs.add(new Point(-1, -10));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -10));
		pntCs.add(new Point(2, -10));
		pntCs.add(new Point(2, -9));
		pntCs.add(new Point(4, -11));
		pntCs.add(new Point(7, -11));
		pntCs.add(new Point(5, -9));
		pntCs.add(new Point(10, -8));
		pntCs.add(new Point(4, -2));

		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(4,-2));

		pntCs.add(new Point(4, 1));
		pntCs.add(new Point(1, 3));
		pntCs.add(new Point(1,6));
		pntCs.add(new Point(0,9));

		setCartesians(pntCs);
	}

	@Override
	public boolean isProtected() {
		return getFade() != 255;
	}

	// ==============================================================
	// METHODS 
	// ==============================================================
	@Override
	public void move() {
		super.move();

		if (isProtected()) {
			setFade(getFade() + 3);
		}

		if(!(moveUp || moveDown || moveLeft || moveRight)) {
			setDeltaX(Math.round(getDeltaX() * BRAKE * 100 ) / 100);
			setDeltaY(Math.round(getDeltaY() * BRAKE * 100) / 100);
		}

		//move up
		if (moveUp) {
//			setCenter(new Point(getCenter().x, getCenter().y-10));
			setDeltaY(Math.max(getDeltaY()-5,-10));
		}

		//move down
		if (moveDown) {
//			setCenter(new Point(getCenter().x, getCenter().y+10));
			setDeltaY(Math.min(getDeltaY()+5,10));
		}

		//move left
		if (moveLeft) {
//			setCenter(new Point(getCenter().x-10, getCenter().y));
			setDeltaX(Math.max(getDeltaX()-5,-10));
		}
		//move right
		if (moveRight) {
//			setCenter(new Point(getCenter().x+10, getCenter().y));
			setDeltaX(Math.min(getDeltaX()+5,10));
		}

		Point pnt = getCenter();
		double newXPos = pnt.x + getDeltaX();
		double newYPos = pnt.y + getDeltaY();

		// make sure the falcon does not go beyond the frame
		if (pnt.x >= Game.DIM.width-20) {
			setCenter(new Point(Game.DIM.width-20, pnt.y));

		} else if (pnt.x <= 20 ) {
			setCenter(new Point(20, pnt.y));
		} else if (pnt.y >= Game.DIM.height - 60) {
			setCenter(new Point(pnt.x, Game.DIM.height -60));

		} else if (pnt.y <= 20) {
			setCenter(new Point(pnt.x, 20));
		} else {
			setCenter(new Point((int) newXPos, (int) newYPos));
		}


	} //end move



	//methods for moving the falcon
	public void moveLeft() {
		moveLeft = true;
	}

	public void moveRight() {
		moveRight = true;
	}


	public void moveUp() {
		moveUp = true;
	}

	public void moveDown() {
		moveDown = true;
	}

	public void stopMoving() {
		moveLeft = false;
		moveUp = false;
		moveRight = false;
		moveDown = false;
	}

	public void thrustOn() {
		thrusting = true;
	}

	public void thrustOff() {
		thrusting = false;
	}

	public boolean isTripleBullet() {
		return tripleBullet;
	}

	public void setTripleBullet(boolean tripleBullet) {
		this.tripleBullet = tripleBullet;
	}

	public int getTripleBulletCount() {
		return tripleBulletCount;
	}

	public void setTripleBulletCount(int tripleBulletCount) {
		this.tripleBulletCount = tripleBulletCount;
	}

	private int adjustColor(int colorNum, int adjust) {
		return Math.max(colorNum - adjust, 0);
	}

	public void fireWeapon() {
		if(tripleBullet) {
			for(int i = 0; i < 3; i++) {
				Bullet bullet = new Bullet(this);
				bullet.setDeltaX(bullet.getDeltaX() + (-20 + i*20));
				bullet.setOrientation(240+i*30);
				CommandCenter.getInstance().getOpsList().enqueue(bullet, CollisionOp.Operation.ADD);
				setTripleBulletCount(getTripleBulletCount()-1);
				if(getTripleBulletCount() == 0) setTripleBullet(false);
			}
		}
		else {
			CommandCenter.getInstance().getOpsList().enqueue(new Bullet(this), CollisionOp.Operation.ADD);
		}
	}


	@Override
	public void draw(Graphics g) {

		Color colShip;
		if (getFade() == 255) {
			colShip = Color.green;
		} else {
			colShip = new Color(

					adjustColor(getFade(), 200), //red
					adjustColor(getFade(), 175), //green
					getFade() //blue
			);
		}

		//most Sprites do not have flames, but Falcon does
		 double[] flames = { 23 * Math.PI / 24 + Math.PI / 2, Math.PI + Math.PI / 2, 25 * Math.PI / 24 + Math.PI / 2 };
		 Point[] pntFlames = new Point[flames.length];

		//thrusting
		if (thrusting) {
			g.setColor(colShip);
			//the flame
			for (int nC = 0; nC < flames.length; nC++) {
				if (nC % 2 != 0) //odd
				{
					//adjust the position so that the flame is off-center
					pntFlames[nC] = new Point((int) (getCenter().x + 2
							* getRadius()
							* Math.sin(Math.toRadians(getOrientation())
									+ flames[nC])), (int) (getCenter().y - 2
							* getRadius()
							* Math.cos(Math.toRadians(getOrientation())
									+ flames[nC])));

				} else //even
				{
					pntFlames[nC] = new Point((int) (getCenter().x + getRadius()
							* 1.1
							* Math.sin(Math.toRadians(getOrientation())
									+ flames[nC])),
							(int) (getCenter().y - getRadius()
									* 1.1
									* Math.cos(Math.toRadians(getOrientation())
											+ flames[nC])));

				} //end even/odd else
			} //end for loop

			g.fillPolygon(
					Arrays.stream(pntFlames)
							.map(pnt -> pnt.x)
							.mapToInt(Integer::intValue)
							.toArray(),

					Arrays.stream(pntFlames)
							.map(pnt -> pnt.y)
							.mapToInt(Integer::intValue)
							.toArray(),

					flames.length);

		} //end if flame

		draw(g,colShip);

	} //end draw()

} //end class
