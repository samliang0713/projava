package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public abstract class Floater extends Sprite {
	private Point[] symbolCartesians;


	public Floater() {

		super();
		setTeam(Team.FLOATER);

		setExpiry(250);
		setRadius(20);
		setColor(Color.yellow);

		// emerges from the edge of the screen
		int randomInt = Game.R.nextInt( 4);;
		double randomDouble = Game.R.nextDouble( );
		switch (randomInt) {
			case 0:
				// Left
				setCenter(new Point(15, (int) (Game.DIM.height*randomDouble)));
				setDeltaX(2+Game.R.nextInt(2));
				setDeltaY(randomDouble < 0.5 ? -3 : 3);
				break;
			case 1:
				// Right
				setCenter(new Point(Game.DIM.width-15, (int) (Game.DIM.height*randomDouble)));
				setDeltaX(-2-Game.R.nextInt(2));
				setDeltaY(randomDouble < 0.5 ? 3 : -3);
				break;
			case 2:
				// Top
				setCenter(new Point((int) (Game.DIM.width*randomDouble),15));
				setDeltaY(2+Game.R.nextInt(2));
				setDeltaX(randomDouble < 0.5 ? 3 : -3);
				break;
			case 3:
				// Bottom
				setCenter(new Point((int) (Game.DIM.width*randomDouble),Game.DIM.height-15));
				setDeltaY(-2-Game.R.nextInt(2));
				setDeltaX(randomDouble <  0.5 ? 3 : -3);
				break;
		}

		setOrientation(90);

		//always set cartesian points last
		List<Point> pntCs = new ArrayList<>();
//		pntCs.add(new Point(5, 5));
//		pntCs.add(new Point(4,0));
//		pntCs.add(new Point(5, -5));
//		pntCs.add(new Point(0,-4));
//		pntCs.add(new Point(-5, -5));
//		pntCs.add(new Point(-4,0));
//		pntCs.add(new Point(-5, 5));
//		pntCs.add(new Point(0,4));

		pntCs.add(new Point(6, 6));
		pntCs.add(new Point(6, -6));
		pntCs.add(new Point(-6, -6));
		pntCs.add(new Point(-6, 6));
		setCartesians(pntCs);
	}

	public Point[] getSymbolCartesians() {
		return symbolCartesians;
	}

	public void setSymbolCartesians(Point[] symbolCartesians) {
		this.symbolCartesians = symbolCartesians;
	}

	public void setSymbolCartesians(List<Point> pntPs) {
		setSymbolCartesians(pntPs.stream()
				.toArray(Point[]::new));

	}

	@Override
	public void move() {
		super.move();

		//and it also expires
		expire();

		// remove floater from game if it reaches the bound of the frame
		double posX = getCenter().x;
		double posY = getCenter().y;

		if(posY <= 0 || posX <= 0 || posX >= Game.DIM.width || posY >= Game.DIM.height) {
			removeFromScreen();
		}
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);

		g.setColor(Color.yellow);

		List<PolarPoint> polars = cartesianToPolar(Arrays.asList(getSymbolCartesians()));

		Function<PolarPoint,Point> adjustPointFunction =
				pp -> new Point(
						(int) (getCenter().x + pp.getR() * getRadius()
								* Math.sin(Math.toRadians(getOrientation())
								+ pp.getTheta())),

						(int) (getCenter().y - pp.getR() * getRadius()
								* Math.cos(Math.toRadians(getOrientation())
								+ pp.getTheta())));


		g.fillPolygon(
				polars.stream()
						.map(adjustPointFunction)
						.map(pnt -> pnt.x)
						.mapToInt(Integer::intValue)
						.toArray(),

				polars.stream()
						.map(adjustPointFunction)
						.map(pnt -> pnt.y)
						.mapToInt(Integer::intValue)
						.toArray(),

				getSymbolCartesians().length);

		//for debugging center-point. Feel free to remove these two lines.
		//#########################################
		g.setColor(Color.ORANGE);
		g.fillOval(getCenter().x -1, getCenter().y -1, 2,2);
		}
	}


