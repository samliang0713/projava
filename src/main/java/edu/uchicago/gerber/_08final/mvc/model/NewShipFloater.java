package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class NewShipFloater extends Floater{
    public NewShipFloater() {
        super();
        List<Point> healthSymbol = new ArrayList<>();

        healthSymbol.add(new Point(-2, -6));
        healthSymbol.add(new Point(-2, -2));
        healthSymbol.add(new Point(-6, -2));
        healthSymbol.add(new Point(-6, 2));

        healthSymbol.add(new Point(-2, 2));
        healthSymbol.add(new Point(-2, 6));
        healthSymbol.add(new Point(2, 6));
        healthSymbol.add(new Point(2, 2));
        healthSymbol.add(new Point(6, 2));
        healthSymbol.add(new Point(6, -2));
        healthSymbol.add(new Point(2, -2));
        healthSymbol.add(new Point(2, -6));


        setSymbolCartesians(healthSymbol);

    }

}
