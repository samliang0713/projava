package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EnemyBullet extends Sprite {

    private final double FIRE_POWER = 15.0;


    public EnemyBullet(EnemyFalcon enemyFalcon) {
        super();

        setTeam(Team.FOE);

        //a bullet expires after 20 frames
        setExpiry(20);
        setRadius(6);

        //everything is relative to the enemy falcon ship that fired the bullet
        setDeltaX(enemyFalcon.getDeltaX() +
                Math.cos(Math.toRadians(enemyFalcon.getOrientation())) * FIRE_POWER);
        setDeltaY(enemyFalcon.getDeltaY() +
                Math.sin(Math.toRadians(enemyFalcon.getOrientation())) * FIRE_POWER);
        setCenter(enemyFalcon.getCenter());


        //set the bullet orientation to the falcon (ship) orientation
        setOrientation(enemyFalcon.getOrientation());

        //make sure to setCartesianPoints last
        //defined the points on a cartesian grid
        List<Point> pntCs = new ArrayList<>();

        pntCs.add(new Point(0, 3)); //top point

        pntCs.add(new Point(1, -1));
        pntCs.add(new Point(0, -2));
        pntCs.add(new Point(-1, -1));
        setCartesians(pntCs);
    }

    @Override
    public void move() {
        super.move();
        expire();
    }
}
