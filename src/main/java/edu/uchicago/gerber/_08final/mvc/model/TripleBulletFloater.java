package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TripleBulletFloater extends Floater {
    public TripleBulletFloater () {
        super();
        List<Point> bulletSymbol = new ArrayList<>();
        bulletSymbol.add(new Point(0, 2));
        bulletSymbol.add(new Point(1, -1));
        bulletSymbol.add(new Point(0, -2));
        bulletSymbol.add(new Point(-1, -1));
        setSymbolCartesians(bulletSymbol);
    }

}
