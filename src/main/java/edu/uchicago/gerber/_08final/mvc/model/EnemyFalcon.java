package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EnemyFalcon extends Sprite {
    private Falcon falcon;
    private final double BRAKE = 0.65;
    private boolean firing;
    private static final double FIRING_PROB = .15;
    private static final int FRAMES_BETWEEN_BULLETS = 3;
    private int framesSinceLastBullet;



    public EnemyFalcon(Falcon falcon) {
        super();
        this.falcon = falcon;

        setTeam(Team.FOE);
        setColor(Color.red);
        setFramesSinceLastBullet(0);

        // emerge from the top
        setCenter(new Point(Game.R.nextInt(Game.DIM.width), 0));

        // pointing down
        setOrientation(90);

        // size of the enemy falcon
        setRadius(40);

        setDeltaY(2);
        setFiring(false);
        setSpriteScore(200);

        //be sure to set cartesian points last.
        List<Point> pntCs = new ArrayList<>();
        // Robert Alef's awesome falcon design
        pntCs.add(new Point(0,9));
        pntCs.add(new Point(-1, 6));
        pntCs.add(new Point(-1,3));
        pntCs.add(new Point(-4, 1));
        pntCs.add(new Point(4,1));
        pntCs.add(new Point(-4,1));

        pntCs.add(new Point(-4, -2));
        pntCs.add(new Point(-1, -2));
        pntCs.add(new Point(-1, -9));
        pntCs.add(new Point(-1, -2));
        pntCs.add(new Point(-4, -2));

        pntCs.add(new Point(-10, -8));
        pntCs.add(new Point(-5, -9));
        pntCs.add(new Point(-7, -11));
        pntCs.add(new Point(-4, -11));
        pntCs.add(new Point(-2, -9));
        pntCs.add(new Point(-2, -10));
        pntCs.add(new Point(-1, -10));
        pntCs.add(new Point(-1, -9));
        pntCs.add(new Point(1, -9));
        pntCs.add(new Point(1, -10));
        pntCs.add(new Point(2, -10));
        pntCs.add(new Point(2, -9));
        pntCs.add(new Point(4, -11));
        pntCs.add(new Point(7, -11));
        pntCs.add(new Point(5, -9));
        pntCs.add(new Point(10, -8));
        pntCs.add(new Point(4, -2));

        pntCs.add(new Point(1, -2));
        pntCs.add(new Point(1, -9));
        pntCs.add(new Point(1, -2));
        pntCs.add(new Point(4,-2));

        pntCs.add(new Point(4, 1));
        pntCs.add(new Point(1, 3));
        pntCs.add(new Point(1,6));
        pntCs.add(new Point(0,9));

        setCartesians(pntCs);
    }

    public EnemyFalcon(Falcon falcon, double xPos, double yPos) {
        this(falcon);
        setCenter(new Point((int) xPos, (int) yPos));
    }

    @Override
    public void move() {
        super.move();

        int deltaX = falcon.getCenter().x - this.getCenter().x;
        int deltaY = falcon.getCenter().y - this.getCenter().y;

        // move towards player falcon

        if(Math.abs(deltaX) < 4) setDeltaX(getDeltaX() * BRAKE);
        else setDeltaX(deltaX < 0 ? -6: 6);
        if(Math.abs(deltaY) > 4) setDeltaY(deltaY < 0 ? -4: 4);


        // determines firing
        if(Game.R.nextDouble() < FIRING_PROB) {
            setFiring(true);
        } else {
            setFiring(false);
        }
    }

    public boolean isFiring() {
        return firing;
    }

    public void setFiring(boolean firing) {
        this.firing = firing;
    }

    public int getFramesSinceLastBullet() {
        return framesSinceLastBullet;
    }

    public void setFramesSinceLastBullet(int framesSinceLastBullet) {
        this.framesSinceLastBullet = framesSinceLastBullet;
    }
    public int getFramesBetweenBullets() {
        return FRAMES_BETWEEN_BULLETS;
    }

}
