package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BombFloater extends Floater{

    public BombFloater() {
        super();
        List<Point> bulletSymbol = new ArrayList<>();
        bulletSymbol.add(new Point(0, -6));
        bulletSymbol.add(new Point(-2, -2));
        bulletSymbol.add(new Point(-6, 0));
        bulletSymbol.add(new Point(-2, 2));
        bulletSymbol.add(new Point(0, 6));
        bulletSymbol.add(new Point(2, 2));
        bulletSymbol.add(new Point(6, 0));
        bulletSymbol.add(new Point(2, -2));
        setSymbolCartesians(bulletSymbol);
    }
}
