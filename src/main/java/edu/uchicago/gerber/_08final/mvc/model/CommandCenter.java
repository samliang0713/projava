package edu.uchicago.gerber._08final.mvc.model;


import edu.uchicago.gerber._08final.mvc.controller.Game;
import edu.uchicago.gerber._08final.mvc.controller.Sound;
import lombok.Data;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

//the lombok @Data gives us automatic getters and setters on all members
@Data
public class CommandCenter {

	private  int numFalcons;
	private int numAsteroids;
	private int numDebris;
	private  int level;
	private  long score;
	//the falcon is located in the movFriends array, but since we use this reference a lot, we keep track of it in a
	//separate reference. See spawnFalcon() method below.

	private  final Falcon falcon = new Falcon();
	private  boolean paused;

	private List<Movable> movDebris = new ArrayList<>(200);
	private List<Movable> movFriends = new ArrayList<>(150);
	private List<Movable> movFoes = new ArrayList<>(200);
	private List<Movable> movFloaters = new ArrayList<>(50);

	private GameOpsList opsList = new GameOpsList();


	private static CommandCenter instance = null;

	// Constructor made private - static Utility class only
	private CommandCenter() {}


	public static CommandCenter getInstance(){
		if (instance == null){
			instance = new CommandCenter();
		}
		return instance;
	}


	public void initGame(){
		setLevel(1);
		setScore(0);
		setNumFalcons(4);
		spawnFalcon();
		spawnInitialAsteroids();
		spawnInitialDebris();
	}

	public void decrementFalcon(){
		setNumFalcons(getNumFalcons() - 1);
		if (isGameOver()) return;
		Sound.playSound("shipspawn.wav");
		falcon.setFade(Falcon.FADE_INITIAL_VALUE);
		falcon.setCenter(new Point(Game.DIM.width / 2, Game.DIM.height / 2));
		falcon.setDeltaX(0);
		falcon.setDeltaY(0);
	}


	public  void spawnFalcon() {
		setNumFalcons(getNumFalcons() - 1);
		if(isGameOver()) return;

		if(!movFriends.contains(falcon)) {
			opsList.enqueue(falcon, CollisionOp.Operation.ADD);
		}


		Sound.playSound("shipspawn.wav");

	}

	public  void clearAll(){
		movDebris.clear();
		movFriends.clear();
		movFoes.clear();
		movFloaters.clear();
	}


	public  boolean isGameOver() {		//if the number of falcons is zero, then game over
		return getNumFalcons() == 0;
	}

	public int getNumAsteroids() {
		return numAsteroids;
	}

	public void setNumAsteroids(int numAsteroids) {
		this.numAsteroids = numAsteroids;
	}

	public int getNumDebris() {
		return numDebris;
	}

	public void setNumDebris(int numDebris) {
		this.numDebris = numDebris;
	}


	public void spawnInitialAsteroids() {
		while (CommandCenter.getInstance().getNumAsteroids() < Asteroid.MAX_ASTEROIDS){

			spawnAsteroid();
		}
	}
	public void spawnAsteroid() {
		CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		CommandCenter.getInstance().setNumAsteroids(CommandCenter.getInstance().getNumAsteroids()+1);
	}

	public void spawnInitialDebris() {
		while (CommandCenter.getInstance().getNumDebris() < Debris.MAX_DEBRIS){
			spawnDebris();
		}
	}

	public void spawnDebris() {
		CommandCenter.getInstance().getOpsList().enqueue(new Debris(), CollisionOp.Operation.ADD);
		CommandCenter.getInstance().setNumDebris(CommandCenter.getInstance().getNumDebris()+1);
	}


	public List<Movable> getMov() {
		List<Movable> movList = new ArrayList<>();
		movList.addAll(movDebris);
		movList.addAll(movFriends);
		movList.addAll(movFoes);
		movList.addAll(movFloaters);
		return movList;
	}
}
