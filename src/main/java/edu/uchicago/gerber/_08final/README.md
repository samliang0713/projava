# Asteroids (Revised Version) 

This game extends the code base provided by Professor Gerber in his Java programming class.

## Control
Use arrow keys to move the falcon, and space bar to fire bullets.

## Implemented Features
### Debris
Debris is an object on the screen that does not interact with either the Friends team or the Foes team. For example, small asteroids 
will be broken into asteroids that are no longer threats to players. The same is true for debris of enemy falcons and
randomly spawned space junk (with dark blue color).

### Floaters
Three kinds of floaters (yellow) are randomly spawned throughout the game at an increasing frequency as player levels up.

* **New Ship Floater:** New ship floaters give player extra lives.

* **Triple Bullet Floater:** Triple bullet floaters allows player falcon to fire three bullets at the same time.

* **Bomb Floater:** Bomb floaters creates a sonic bomb around player falcon and eliminates nearby foes.

### Enemy Falcons
Enemy falcons will be spawned throughout the game. En**emy falcons can fire and will attempt to crash into player falcons.

### Scores & Levels
Destroying foes earns player scores. Different units of foes (big asteroids, small asteroids, enemy falcons) have
different scores. Levels are determined by scores. The frequency at which floaters, asteroids, and enemy falcons are 
spawned changes as player level up.
