package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Initialization
        boolean yearExists = false;
        int year = -1;

        //Prompt year input
        while (!yearExists) {
            try {
                System.out.print("Please enter a year: ");
                year = scanner.nextInt();
                yearExists = true;
            } catch (Exception e) {
                System.out.println("Please enter an integer");
                scanner.next();
            }
        }

        //Determine if input is leap year according to rules
        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is not a leap year");
        }
    }
}
