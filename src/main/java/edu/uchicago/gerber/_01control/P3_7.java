package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        //Initialization
        boolean incomeExists = false;
        double income = -1;

        //Prompt income
        while (!incomeExists) {
            try {
                System.out.print("Please enter a income: ");
                income = scanner.nextDouble();
                if (income > 0) incomeExists = true;
                else System.out.println("Please enter a valid income");
            } catch (Exception e) {
                System.out.println("Please enter a floating-point number");
                scanner.next();
            }
        }

        //Tax calculation
        double tax;
        if (income > 500_000) {
            tax = 250_000 * 0.05 + 150_000 * 0.04 + 25_000 * 0.03 + 25_000 * 0.02 + 50_000 * 0.01;
            tax += (income - 500_000) * 0.06;
        } else if (income > 250_000) {
            tax = 150_000 * 0.04 + 25_000 * 0.03 + 25_000 * 0.02 + 50_000 * 0.01;
            tax += (income - 250_000) * 0.05;
        } else if (income > 100_000) {
            tax = 25_000 * 0.03 + 25_000 * 0.02 + 50_000 * 0.01;
            tax += (income - 100_000) * 0.04;
        } else if (income > 75_000) {
            tax = 25_000 * 0.02 + 50_000 * 0.01;
            tax += (income - 75_000) * 0.03;
        } else if (income > 50_000) {
            tax = 50_000 * 0.01;
            tax += (income - 50_000) * 0.02;
        } else {
            tax = income * 0.01;
        }

        //Output
        System.out.println("Tax is: " + tax);

    }
}
