package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_6 {
    public static final double METERS_TO_MILES = 0.000621371;
    public static final double METERS_TO_FEET = 3.28084;
    public static final double METERS_TO_INCHES = 39.3701;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Initialization
        boolean measureExists = false;
        double measure = -1;

        //Prompt measurement in meters input
        while (!measureExists) {
            try {
                System.out.print("Type measurement in meters: ");
                measure = scanner.nextDouble();
                measureExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a number!");
                scanner.next();
            }
        }

        //Output
        System.out.println("Measure in miles is: " + measure * METERS_TO_MILES);
        System.out.println("Measure in feet is: " + measure * METERS_TO_FEET);
        System.out.println("Measure in inches is: " + measure * METERS_TO_INCHES);
    }
}
