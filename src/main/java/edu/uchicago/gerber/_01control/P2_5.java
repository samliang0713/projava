package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P2_5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Initialization
        boolean priceExists = false;
        double price = -1;
        int dollars;
        int cents;

        //Prompts price
        while (!priceExists) {
            try {
                System.out.print("Please enter a price: ");
                price = scanner.nextDouble();
                priceExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a floating-point number");
                scanner.next();
            }
        }

        //Extract dollars and cents according to the pseudocode
        dollars = (int) price;
        cents = (int) ((price - dollars) * 100 + 0.5);

        //Output
        System.out.println("The dollars are: " + dollars);
        System.out.println("The cents are: " + cents);
    }
}
