package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Initialization
        boolean integerExists = false;
        int integer = -1;

        //Prompt integer input
        while (!integerExists) {
            try {
                System.out.print("Please enter a positive integer : ");
                integer = scanner.nextInt();
                if (integer >= 1 && integer <= 3999) integerExists = true;
                else System.out.println("Please enter a valid integer between 1 and 3999");
            } catch (Exception e) {
                System.out.println("Please enter an integer between 1 and 3999");
                scanner.next();
            }
        }

        //Extract digits from the input
        int thousands = integer / 1000;
        int hundreds = (integer - thousands * 1000) / 100;
        int tens = (integer - thousands * 1000 - hundreds * 100) / 10;
        int ones = integer % 10;

        //Initialize roman expression string

        String romanExpression = "";


        //Add roman numerals to the expression according to rules
        if (thousands == 1) {
            romanExpression += "M";
        } else if (thousands == 2) {
            romanExpression += "MM";
        } else if (thousands == 3) {
            romanExpression += "MMM";
        }

        if (hundreds == 1) {
            romanExpression += "C";
        } else if (hundreds == 2) {
            romanExpression += "CC";
        } else if (hundreds == 3) {
            romanExpression += "CCC";
        } else if (hundreds == 4) {
            romanExpression += "CD";
        } else if (hundreds == 5) {
            romanExpression += "D";
        } else if (hundreds == 6) {
            romanExpression += "DC";
        } else if (hundreds == 7) {
            romanExpression += "DCC";
        } else if (hundreds == 8) {
            romanExpression += "DCCC";
        } else if (hundreds == 9) {
            romanExpression += "CM";
        }

        if (tens == 1) {
            romanExpression += "X";
        } else if (tens == 2) {
            romanExpression += "XX";
        } else if (tens == 3) {
            romanExpression += "XXX";
        } else if (tens == 4) {
            romanExpression += "XL";
        } else if (tens == 5) {
            romanExpression += "L";
        } else if (tens == 6) {
            romanExpression += "LX";
        } else if (tens == 7) {
            romanExpression += "LXX";
        } else if (tens == 8) {
            romanExpression += "LXXX";
        } else if (tens == 9) {
            romanExpression += "XC";
        }

        if (ones == 1) {
            romanExpression += "I";
        } else if (ones == 2) {
            romanExpression += "II";
        } else if (ones == 3) {
            romanExpression += "III";
        } else if (ones == 4) {
            romanExpression += "IV";
        } else if (ones == 5) {
            romanExpression += "V";
        } else if (ones == 6) {
            romanExpression += "VI";
        } else if (ones == 7) {
            romanExpression += "VII";
        } else if (ones == 8) {
            romanExpression += "VIII";
        } else if (ones == 9) {
            romanExpression += "IX";
        }

        //Output
        System.out.println("The Roman expression for " + integer + " is: " + romanExpression);
    }
}
