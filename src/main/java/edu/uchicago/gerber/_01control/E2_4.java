package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_4 {

    public static void main(String[] args) {


        //Initialization
        boolean firstIntExists = false;
        boolean secondIntExists = false;
        int firstInt = -1;
        int secondInt = -1;


        Scanner scanner = new Scanner(System.in);

        //Prompt first integer input
        while (!firstIntExists) {
            try {
                System.out.println("Type 1st integer: ");
                firstInt = scanner.nextInt();
                firstIntExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a integer!");
                scanner.next();
            }
        }
        //Prompt second integer input
        while (!secondIntExists) {
            try {
                System.out.println("Type 2nd integer: ");
                secondInt = scanner.nextInt();
                secondIntExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a integer!");
                scanner.next();
            }
        }


        //Output
        System.out.println("Sum: " + (firstInt + secondInt));
        System.out.println("Difference: " + (firstInt - secondInt));
        System.out.println("Product: " + firstInt * secondInt);
        System.out.println("Average: " + (firstInt + secondInt) / 2.0);
        System.out.println("Distance: " + Math.abs(firstInt - secondInt));
        System.out.println("Maximum: " + Math.max(firstInt, secondInt));
        System.out.println("Minimum: " + Math.min(firstInt, secondInt));


    }
}
