package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Initialization
        boolean inputExists = false;
        int input = -1;

        //Prompt input
        while (!inputExists) {
            try {
                System.out.print("Please enter a integer: ");
                input = scanner.nextInt();
                inputExists = true;

            } catch (Exception e) {
                System.out.println("Please enter an integer");
                scanner.next();
            }
        }

        //Save a copy of original input and initialization an empty string
        int originalInput = input;
        String binary = "";

        //Concatenate digit to the binary string
        while (input > 0) {
            int digit = input % 2;
            binary = digit + binary;
            input = input / 2;
        }

        //Output
        System.out.println("The binary input of " + originalInput + " is " + binary);
    }
}
