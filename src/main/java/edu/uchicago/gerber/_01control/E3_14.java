package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        //Initialization
        boolean dayExists = false;
        boolean monthExists = false;
        int month = -1;
        int day = -1;
        String season = "";


        //Prompt month input
        while (!monthExists) {
            try {
                System.out.print("Please enter a month : ");
                month = scanner.nextInt();
                if (month >= 1 && month <= 12) monthExists = true;
                else System.out.println("Please enter a valid month");
            } catch (Exception e) {
                System.out.println("Please enter an integer between 1 and 12");
                scanner.next();
            }
        }

        //Prompt day input
        while (!dayExists) {
            try {
                System.out.print("Please enter a day : ");
                day = scanner.nextInt();
                if (day >= 1 && day <= 31) dayExists = true;
                else System.out.println("Please enter a valid day");
            } catch (Exception e) {
                System.out.println("Please enter an integer between 1 and 31");
                scanner.next();
            }
        }

        // Determine Season
        switch (month) {
            case 1:
            case 2:
            case 3:
                season = "Winter";
                break;
            case 4:
            case 5:
            case 6:
                season = "Spring";
                break;
            case 7:
            case 8:
            case 9:
                season = "Summer";
                break;
            case 10:
            case 11:
            case 12:
                season = "Fall";
                break;
        }
        if (month % 3 == 0 && day >= 21) {
            switch (season) {
                case "Winter":
                    season = "Spring";
                    break;
                case "Spring":
                    season = "Summer";
                    break;
                case "Summer":
                    season = "Fall";
                    break;
                default:
                    season = "Winter";
            }
        }

        //Output
        System.out.println("Month: " + month);
        System.out.println("Day: " + day);
        System.out.println("Season is: " + season);
    }
}
