package edu.uchicago.gerber._01control;

public class E2_20 {

    public static void main(String[] args) {

        //Output
        System.out.println("   /\\   ");
        System.out.println("  /  \\  ");
        System.out.println(" /    \\ ");
        System.out.println("/      \\");
        System.out.println("--------");
        System.out.println("  \"  \"  ");
        System.out.println("  \"  \"  ");
        System.out.println("  \"  \"  ");
    }
}
