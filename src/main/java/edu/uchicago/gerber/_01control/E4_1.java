package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args) {

        //Initialization
        int evenSumTo100 = 0;
        int squareSumTo100 = 0;
        int sumOddFromAtoB = 0;
        int sumOfOddDigits = 0;

        boolean aExists = false;
        boolean bExists = false;
        boolean inputForEExists = false;

        int a = -1;
        int b = -1;
        int inputForE = -1;


        Scanner scanner = new Scanner(System.in);

        // Prompt input for a in question d.
        while (!aExists) {
            try {
                System.out.print("Type an integer for a in question d.: ");
                a = scanner.nextInt();
                aExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a integer!");
                scanner.next();
            }
        }
        // Prompt input for b in question d.
        while (!bExists) {
            try {
                System.out.print("Type an integer for b in question d.: ");
                b = scanner.nextInt();
                bExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a integer!");
                scanner.next();
            }
        }
        // Prompt input for question e.
        while (!inputForEExists) {
            try {
                System.out.print("Type an integer in question e.: ");
                inputForE = scanner.nextInt();
                inputForEExists = true;
            } catch (Exception e) {
                System.out.println("Please enter a integer!");
                scanner.next();
            }
        }

        // calculates sum for question a. and question b.
        for (int i = 0; i <= 100; i++) {
            squareSumTo100 += i * i;
            if (i % 2 == 0) {
                evenSumTo100 += i;
            }
        }

        // calculates sum for question d.
        for (int i = Math.min(a, b); i <= Math.max(a, b); i++) {
            if (i % 2 != 0) sumOddFromAtoB += i;
        }


        // saves a copy of original input for question e. and calculates number of digits of it
        int originalInputForE = inputForE;
        int numberDigits = String.valueOf(inputForE).length();

        //Extract every digit of the input and add to sum if the digit is odd
        while (numberDigits > 0) {
            int digit = (int) (inputForE / Math.pow(10, numberDigits - 1));
            if (digit % 2 != 0) sumOfOddDigits += digit;
            inputForE = (int) (inputForE % Math.pow(10, numberDigits - 1));
            numberDigits--;
        }


        //Output for question a. and b.
        System.out.println("The sum of all even numbers between 2 and 100 is: " + evenSumTo100);
        System.out.println("The sum of all squares between 1 and 100 is: " + squareSumTo100);

        //Output for question c.
        for (int i = 0; i <= 20; i++) {
            System.out.println(i + " power of 2: " + Math.pow(2, i));
        }

        //Output for question d. and e.
        System.out.println("The sum of all odd numbers between " + a + " and " + b + " is: " + sumOddFromAtoB);
        System.out.println("The sum of all odd digits of " + originalInputForE + " is: " + sumOfOddDigits);
    }
}
